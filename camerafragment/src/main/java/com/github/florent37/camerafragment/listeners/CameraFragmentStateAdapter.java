/*
 * Copyright (C) 2021 Huawei Device Co., Ltd.
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 */

package com.github.florent37.camerafragment.listeners;

import java.io.File;

/**
 * 相机Fraction状态adapter
 *
 * @since 2021-05-25
 */
public class CameraFragmentStateAdapter implements CameraFragmentStateListener {
    @Override
    public void onCurrentCameraBack() {
    }

    @Override
    public void onCurrentCameraFront() {
    }

    @Override
    public void onFlashAuto() {
    }

    @Override
    public void onFlashOn() {
    }

    @Override
    public void onFlashOff() {
    }

    @Override
    public void onCameraSetupForPhoto() {
    }

    @Override
    public void onCameraSetupForVideo() {
    }

    @Override
    public void onRecordStateVideoReadyForRecord() {
    }

    @Override
    public void onRecordStateVideoInProgress() {
    }

    @Override
    public void onRecordStatePhoto() {
    }

    @Override
    public void shouldRotateControls(int degrees) {
    }

    @Override
    public void onStartVideoRecord(File outputFile) {
    }

    @Override
    public void onStopVideoRecord() {
    }
}
