/*
 * Copyright (C) 2021 Huawei Device Co., Ltd.
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 */

package com.github.florent37.camerafragment.widgets;

import com.github.florent37.camerafragment.ResourceTable;

import ohos.agp.components.AttrSet;
import ohos.agp.components.Button;
import ohos.agp.components.Component;
import ohos.agp.components.element.ShapeElement;
import ohos.app.Context;
import ohos.multimodalinput.event.TouchEvent;

/**
 * 自定义拍照按钮
 *
 * @since 2021-05-25
 */
public class CameraPhotoView extends Button implements Component.ClickedListener, Component.TouchEventListener {
    private static final int CONSTANT_2 = 2;
    private long lastClickTime = 0L;
    private float downY;
    private float downX;
    private ShapeElement normalShape;
    private ShapeElement selectedShape;
    private RecordButtonListener listener;

    /**
     * 构造函数
     *
     * @param context 上下文
     */
    public CameraPhotoView(Context context) {
        this(context, null);
    }

    /**
     * 构造函数
     *
     * @param context 上下文
     * @param attrSet attr集合
     */
    public CameraPhotoView(Context context, AttrSet attrSet) {
        this(context, attrSet, null);
    }

    /**
     * 构造函数
     *
     * @param context 上下文
     * @param attrSet attr集合
     * @param styleName 类型名字
     */
    public CameraPhotoView(Context context, AttrSet attrSet, String styleName) {
        super(context, attrSet, styleName);
        normalShape = new ShapeElement(getContext(),
            ResourceTable.Graphic_background_component_photo_button_normal);
        selectedShape = new ShapeElement(getContext(),
            ResourceTable.Graphic_background_component_photo_button_selected);
        setBackground(normalShape);
        setTouchEventListener(this);
    }

    @Override
    public void onClick(Component component) {
        final int clickDelay = 1000;
        if (System.currentTimeMillis() - lastClickTime < clickDelay) {
            return;
        } else {
            lastClickTime = System.currentTimeMillis();
        }

        if (listener != null) {
            listener.onRecordButtonClicked();
        }
    }

    /**
     * onTouch方法
     *
     * @param component 视图
     * @param touchEvent 触摸事件
     * @return boolean是否消费
     */
    @Override
    public boolean onTouchEvent(Component component, TouchEvent touchEvent) {
        switch (touchEvent.getAction()) {
            case TouchEvent.PRIMARY_POINT_DOWN: // 手指点下
                downX = touchEvent.getPointerScreenPosition(0).getX();
                downY = touchEvent.getPointerScreenPosition(0).getY();
                setBackground(selectedShape);
                break;
            case TouchEvent.POINT_MOVE: // 手指在移动
                float moveX = touchEvent.getPointerScreenPosition(0).getX();
                float moveY = touchEvent.getPointerScreenPosition(0).getY();
                float width = getWidth();
                float height = getHeight();
                if (Math.abs(moveX - downX) > width / CONSTANT_2
                    || Math.abs(moveY - downY) > height / CONSTANT_2) {
                    setBackground(normalShape); // 取消当前图片颜色,恢复白色图片
                }
                break;
            case TouchEvent.PRIMARY_POINT_UP: // 手指抬起来
                setBackground(normalShape);
                break;
            default:
                break;
        }
        return true;
    }

    /**
     * 录制按钮点击监听
     *
     * @since 2021-05-25
     */
    public interface RecordButtonListener {
        /**
         * 录制按钮点击
         */
        void onRecordButtonClicked();
    }

    /**
     * 设置按钮监听方法
     *
     * @param buttonListener 录制监听
     */
    public void setRecordButtonListener(RecordButtonListener buttonListener) {
        this.listener = buttonListener;
    }
}
