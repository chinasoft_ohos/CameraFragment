/*
 * Copyright (C) 2021 Huawei Device Co., Ltd.
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 */

package com.github.florent37.camerafragment.internal.enums;

import com.github.florent37.camerafragment.configuration.Configuration;

import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;

/**
 * 媒体操作
 *
 * @since 2021-05-24
 */
public class MediaAction {
    /**
     * 拍照
     */
    public static final int ACTION_PHOTO = 0;
    /**
     * 录像
     */
    public static final int ACTION_VIDEO = 1;

    /**
     * 构造函数
     */
    private MediaAction() {
    }

    /**
     * 媒体操作注解
     *
     * @since 2021-05-24
     */
    @Configuration.IntDef({ACTION_PHOTO, ACTION_VIDEO})
    @Retention(RetentionPolicy.SOURCE)
    public @interface MediaActionState {
    }
}
