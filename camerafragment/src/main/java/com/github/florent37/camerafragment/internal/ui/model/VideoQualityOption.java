/*
 * Copyright (C) 2021 Huawei Device Co., Ltd.
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 */

package com.github.florent37.camerafragment.internal.ui.model;

import com.github.florent37.camerafragment.configuration.Configuration;

import ohos.media.recorder.RecorderProfile;

import java.util.concurrent.TimeUnit;

/**
 * 视频分辨率选择
 *
 * @since 2021-05-25
 */
public class VideoQualityOption implements CharSequence {
    private static final int CONSTANT_10 = 10;
    private static final int CONSTANT_60 = 60;
    private static final String ZERO = "0";
    private String title;
    @Configuration.MediaQuality
    private int mediaQuality;

    /**
     * 构造函数
     *
     * @param mediaQuality 质量类型
     * @param camcorderProfile 录像概要文件
     * @param videoDuration 录像时长
     */
    public VideoQualityOption(int mediaQuality, RecorderProfile camcorderProfile, double videoDuration) {
        this.mediaQuality = mediaQuality;

        long minutes = TimeUnit.SECONDS.toMinutes((long) videoDuration);
        long seconds = ((long) videoDuration) - minutes * CONSTANT_60;

        if (mediaQuality == Configuration.MEDIA_QUALITY_AUTO) {
            title = "Auto " + ", (" + (minutes > CONSTANT_10
                ? minutes : (ZERO + minutes)) + ":"
                + (seconds > CONSTANT_10 ? seconds : (ZERO + seconds)) + " min)";
        } else {
            title = String.valueOf(camcorderProfile.vFrameWidth)
                + " X " + String.valueOf(camcorderProfile.vFrameHeight)
                + (videoDuration <= 0 ? "" : ", (" + (minutes > CONSTANT_10
                ? minutes : (ZERO + minutes)) + ":"
                + (seconds > CONSTANT_10 ? seconds : (ZERO + seconds)) + " min)");
        }
    }

    @Configuration.MediaQuality
    public int getMediaQuality() {
        return mediaQuality;
    }

    @Override
    public int length() {
        return title.length();
    }

    @Override
    public char charAt(int index) {
        return title.charAt(index);
    }

    @Override
    public CharSequence subSequence(int start, int end) {
        return title.subSequence(start, end);
    }

    @Override
    public String toString() {
        return title;
    }
}