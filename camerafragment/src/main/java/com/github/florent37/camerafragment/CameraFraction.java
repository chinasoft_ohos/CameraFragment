/*
 * Copyright (C) 2021 Huawei Device Co., Ltd.
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 */

package com.github.florent37.camerafragment;

import com.github.florent37.camerafragment.configuration.Configuration;
import com.github.florent37.camerafragment.internal.ui.BaseAnncaFraction;

/**
 * 相机Fraction页面创建
 *
 * @since 2021-05-21
 */
public class CameraFraction extends BaseAnncaFraction {
    /**
     * 实例化Fraction
     *
     * @param configuration 配置项
     * @return CameraFraction 相机Fraction
     */
    public static CameraFraction newInstance(Configuration configuration) {
        return (CameraFraction) BaseAnncaFraction.newInstance(new CameraFraction(), configuration);
    }
}
