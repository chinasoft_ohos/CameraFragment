/*
 * Copyright (C) 2021 Huawei Device Co., Ltd.
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 */

package com.github.florent37.camerafragment.configuration;

/**
 * 配置常量类
 *
 * @since 2021-03-04
 */
public class ConfigurationProviderImpl implements ConfigurationProvider {
    private static final int DEFAULT = -1;
    private static final int ONE_THOUSAND = 1000;
    private long videoFileSize = DEFAULT;
    private int videoDuration = DEFAULT;
    private int minimumVideoDuration = DEFAULT;
    private int degrees = DEFAULT;

    @Configuration.MediaAction
    private int mediaAction = Configuration.MEDIA_ACTION_UNSPECIFIED;

    @Configuration.MediaQuality
    private int mediaPhotoQuality = Configuration.MEDIA_QUALITY_MEDIUM;

    @Configuration.MediaQuality
    private int mediaVideoQuality = Configuration.MEDIA_QUALITY_MEDIUM;

    @Configuration.MediaQuality
    private int passedMediaQuality = Configuration.MEDIA_QUALITY_MEDIUM;

    @Configuration.FlashMode
    private int flashMode = Configuration.FLASH_MODE_AUTO;

    @Configuration.CameraFace
    private int cameraFace = Configuration.CAMERA_FACE_REAR; // 后置摄像头

    @Configuration.SensorPosition
    private int sensorPosition = Configuration.SENSOR_POSITION_UNSPECIFIED;

    @Configuration.DeviceDefaultOrientation
    private int deviceDefaultOrientation;

    /**
     * 获取媒体操作
     *
     * @return int整数
     */
    @Override
    public int getMediaAction() {
        return mediaAction;
    }

    /**
     * 获取照片画面质量
     *
     * @return int整数
     */
    @Override
    public int getMediaPhotoQuality() {
        return mediaPhotoQuality;
    }

    /**
     * 获取录像画面质量
     *
     * @return int整数
     */
    @Override
    public int getMediaVideoQuality() {
        return mediaVideoQuality;
    }

    /**
     * 获取视频时长
     *
     * @return int整数
     */

    @Override
    public int getVideoDuration() {
        return videoDuration;
    }

    /**
     * 获取视频文件大小
     *
     * @return long类型
     */
    @Override
    public long getVideoFileSize() {
        return videoFileSize;
    }

    /**
     * 获取录像最小时长
     *
     * @return int整数
     */
    @Override
    public int getMinimumVideoDuration() {
        return minimumVideoDuration / ONE_THOUSAND;
    }

    /**
     * 获取传感器位置
     *
     * @return int整数
     */
    @Override
    public int getSensorPosition() {
        return sensorPosition;
    }

    /**
     * 获取旋转角度
     *
     * @return int整数
     */
    @Override
    public int getDegrees() {
        return degrees;
    }

    /**
     * 获取闪光灯模式
     *
     * @return int整数
     */
    @Override
    public int getFlashMode() {
        return flashMode;
    }

    /**
     * 设置图片质量
     *
     * @param mediaQuality 图片画质
     */
    @Override
    public void setMediaPhotoQuality(int mediaQuality) {
        this.mediaPhotoQuality = mediaQuality;
    }

    /**
     * 设置录像画面质量
     *
     * @param videoQuality 视频画质
     */
    @Override
    public void setMediaVideoQuality(int videoQuality) {
        this.mediaVideoQuality = videoQuality;
    }

    /**
     * 设置媒体质量
     *
     * @param mediaQuality 画质
     */
    @Override
    public void setPassedMediaQuality(int mediaQuality) {
        this.passedMediaQuality = mediaQuality;
    }

    /**
     * 设置视频时长
     *
     * @param videoDuration 时长
     */
    @Override
    public void setVideoDuration(int videoDuration) {
        this.videoDuration = videoDuration;
    }

    /**
     * 设置视频文件大小
     *
     * @param videoFileSize 文件大小
     */
    @Override
    public void setVideoFileSize(long videoFileSize) {
        this.videoFileSize = videoFileSize;
    }

    /**
     * 设置视频最小时长
     *
     * @param minimumVideoDuration 视频时长
     */
    @Override
    public void setMinimumVideoDuration(int minimumVideoDuration) {
        this.minimumVideoDuration = minimumVideoDuration;
    }

    /**
     * 设置闪光灯模式
     *
     * @param flashMode 模式
     */
    @Override
    public void setFlashMode(int flashMode) {
        this.flashMode = flashMode;
    }

    /**
     * 设置媒体操作
     *
     * @param mediaAction 媒体操作
     */
    public void setMediaAction(@Configuration.MediaAction int mediaAction) {
        this.mediaAction = mediaAction;
    }

    /**
     * 设置传感器位置
     *
     * @param sensorPosition 传感器位置
     */
    @Override
    public void setSensorPosition(int sensorPosition) {
        this.sensorPosition = sensorPosition;
    }

    /**
     * 获取媒体质量
     *
     * @return int整数
     */
    @Override
    public int getPassedMediaQuality() {
        return passedMediaQuality;
    }

    /**
     * 获取媒体质量
     *
     * @return int整数
     */
    @Configuration.CameraFace
    public int getCameraFace() {
        return cameraFace;
    }

    /**
     * 设置相机前后置
     *
     * @param cameraFace 前后置摄像头
     */
    public void setCameraFace(@Configuration.CameraFace int cameraFace) {
        this.cameraFace = cameraFace;
    }

    /**
     * 设置媒体配置项
     *
     * @param configuration 配置项
     */
    @Override
    public void setupWithAnnaConfiguration(Configuration configuration) {
        if (configuration != null) {
            setMediaActionMethod(configuration);

            setMediaQualityMethod(configuration);

            int duration = configuration.getVideoDuration();
            if (duration != DEFAULT) {
                setVideoDuration(duration);
            }

            int face = configuration.getCameraFace();
            if (face != DEFAULT) {
                setCameraFace(face);
            }

            long fileSize = configuration.getVideoFileSize();
            if (fileSize != DEFAULT) {
                setVideoFileSize(fileSize);
            }

            int videoDuration1 = configuration.getMinimumVideoDuration();
            if (DEFAULT != videoDuration1) {
                setMinimumVideoDuration(videoDuration1);
            }

            setFlashModeMethod(configuration);
        }
    }

    // 设置闪光灯模式
    private void setFlashModeMethod(Configuration configuration) {
        int mode = configuration.getFlashMode();
        if (mode != DEFAULT) {
            switch (mode) {
                case Configuration.FLASH_MODE_ON:
                    setFlashMode(Configuration.FLASH_MODE_ON);
                    break;
                case Configuration.FLASH_MODE_OFF:
                    setFlashMode(Configuration.FLASH_MODE_OFF);
                    break;
                case Configuration.FLASH_MODE_AUTO:
                default:
                    setFlashMode(Configuration.FLASH_MODE_AUTO);
                    break;
            }
        }
    }

    // 设置媒体质量
    private void setMediaQualityMethod(Configuration configuration) {
        int mediaQuality = configuration.getMediaQuality();
        if (mediaQuality != DEFAULT) {
            switch (mediaQuality) {
                case Configuration.MEDIA_QUALITY_AUTO:
                    setMediaPhotoQuality(Configuration.MEDIA_QUALITY_AUTO);
                    setMediaVideoQuality(Configuration.MEDIA_QUALITY_AUTO);
                    break;
                case Configuration.MEDIA_QUALITY_HIGHEST:
                    setMediaPhotoQuality(Configuration.MEDIA_QUALITY_HIGHEST);
                    setMediaVideoQuality(Configuration.MEDIA_QUALITY_HIGHEST);
                    break;
                case Configuration.MEDIA_QUALITY_HIGH:
                    setMediaPhotoQuality(Configuration.MEDIA_QUALITY_HIGH);
                    setMediaVideoQuality(Configuration.MEDIA_QUALITY_HIGH);
                    break;
                case Configuration.MEDIA_QUALITY_LOW:
                    setMediaPhotoQuality(Configuration.MEDIA_QUALITY_LOW);
                    setMediaVideoQuality(Configuration.MEDIA_QUALITY_LOW);
                    break;
                case Configuration.MEDIA_QUALITY_LOWEST:
                    setMediaPhotoQuality(Configuration.MEDIA_QUALITY_LOWEST);
                    setMediaVideoQuality(Configuration.MEDIA_QUALITY_LOWEST);
                    break;
                case Configuration.MEDIA_QUALITY_MEDIUM:
                default:
                    setMediaPhotoQuality(Configuration.MEDIA_QUALITY_MEDIUM);
                    setMediaVideoQuality(Configuration.MEDIA_QUALITY_MEDIUM);
                    break;
            }
            setPassedMediaQuality(getMediaPhotoQuality());
        }
    }

    // 设置媒体操作
    private void setMediaActionMethod(Configuration configuration) {
        int action = configuration.getMediaAction();
        if (action != DEFAULT) {
            switch (action) {
                case Configuration.MEDIA_ACTION_PHOTO:
                    setMediaAction(Configuration.MEDIA_ACTION_PHOTO);
                    break;
                case Configuration.MEDIA_ACTION_VIDEO:
                    setMediaAction(Configuration.MEDIA_ACTION_VIDEO);
                    break;
                default:
                    setMediaAction(Configuration.MEDIA_ACTION_UNSPECIFIED);
                    break;
            }
        }
    }

    /**
     * 获取设备默认方向
     *
     * @return int整数
     */
    public int getDeviceDefaultOrientation() {
        return deviceDefaultOrientation;
    }

    /**
     * 设置设备默认方向
     *
     * @param deviceDefaultOrientation 默认方向
     */
    public void setDeviceDefaultOrientation(int deviceDefaultOrientation) {
        this.deviceDefaultOrientation = deviceDefaultOrientation;
    }

    /**
     * 设置旋转角度
     *
     * @param degrees 旋转角度
     */
    @Override
    public void setDegrees(int degrees) {
        this.degrees = degrees;
    }
}
