/*
 * Copyright (C) 2021 Huawei Device Co., Ltd.
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 */

package com.github.florent37.camerafragment.internal.ui.view;

import com.github.florent37.camerafragment.internal.utils.Log;

import ohos.agp.components.Component;
import ohos.agp.graphics.SurfaceOps;
import ohos.app.Context;

/**
 * 自动拟合纹理视图
 *
 * @since 2021-05-25
 */
public class AutoFitTextureView extends TempSurfaceView implements Component.EstimateSizeListener {
    private static final String TAG = "AutoFitTextureView";

    private final SurfaceOps surfaceHolder;

    private int ratioWidth = 0;
    private int ratioHeight = 0;

    /**
     * 构造函数
     *
     * @param context 上下文
     * @param callback 回调
     */
    public AutoFitTextureView(Context context, SurfaceOps.Callback callback) {
        super(context, null);
        this.surfaceHolder = getHolder();
        this.surfaceHolder.addCallback(callback);
        setEstimateSizeListener(this);
    }

    /**
     * 设置宽高比
     *
     * @param width 宽度
     * @param height 高度
     */
    public void setAspectRatio(int width, int height) {
        if (width < 0 || height < 0) {
            Log.error(TAG, "Size cannot be negative.");
            return;
        }
        ratioWidth = width;
        ratioHeight = height;

        postLayout();
    }

    public int getRatioWidth() {
        return ratioWidth;
    }

    public int getRatioHeight() {
        return ratioHeight;
    }

    @Override
    public boolean onEstimateSize(int widthEstimatedConfig, int heightEstimatedConfig) {
        final int width = EstimateSpec.getSize(widthEstimatedConfig);
        final int height = EstimateSpec.getSize(heightEstimatedConfig);

        if (ratioWidth == 0 || ratioHeight == 0) {
            setEstimatedSize(width, height);
        } else {
            if (width < height * (ratioWidth / (float) ratioHeight)) {
                setEstimatedSize(width, (int) (width * (ratioWidth / (float) ratioHeight)));
            } else {
                setEstimatedSize((int) (height * (ratioWidth / (float) ratioHeight)), height);
            }
        }
        return true;
    }
}
