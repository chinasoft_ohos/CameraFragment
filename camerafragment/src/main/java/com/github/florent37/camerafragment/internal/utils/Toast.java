/*
 * Copyright (C) 2021 Huawei Device Co., Ltd.
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 */

package com.github.florent37.camerafragment.internal.utils;

import ohos.agp.colors.RgbColor;
import ohos.agp.components.AttrHelper;
import ohos.agp.components.DirectionalLayout;
import ohos.agp.components.Text;
import ohos.agp.components.element.ShapeElement;
import ohos.agp.utils.Color;
import ohos.agp.utils.LayoutAlignment;
import ohos.agp.utils.Point;
import ohos.agp.window.dialog.ToastDialog;
import ohos.agp.window.service.Display;
import ohos.agp.window.service.DisplayAttributes;
import ohos.agp.window.service.DisplayManager;
import ohos.app.Context;
import ohos.global.resource.NotExistException;
import ohos.global.resource.WrongTypeException;

import java.io.IOException;
import java.util.Optional;

/**
 * Toast封装
 *
 * @since 2021-05-07
 */
public class Toast {
    private static final String TAG = "Toast";
    private static final int LENGTH_LONG = 4000;
    private static final int LENGTH_SHORT = 2000;
    private static final float WIDTH_RATIO = 0.9f;
    private static final int PADDING_SIZE_10 = 10;
    private static final int PADDING_SIZE_12 = 12;
    private static final int PADDING_SIZE_20 = 20;
    private static final int PADDING_SIZE_32 = 32;
    private static final int PADDING_SIZE_40 = 40;

    private Toast() {
    }

    /**
     * Toast位置
     *
     * @since 2021-05-07
     */
    public enum ToastLayout {
        /**
         * 默认位置
         */
        DEFAULT,
        /**
         * 中间
         */
        CENTER,
        /**
         * 顶部
         */
        TOP,
        /**
         * 底部
         */
        BOTTOM,
    }

    /**
     * Toast显示短时间
     *
     * @param context 上下文
     * @param content 内容
     */
    public static void showShort(Context context, String content) {
        createToast(context, content, LENGTH_SHORT, ToastLayout.DEFAULT);
    }

    /**
     * Toast 显示长时间
     *
     * @param context 上下文
     * @param content 内容
     */
    public static void showLong(Context context, String content) {
        createToast(context, content, LENGTH_LONG, ToastLayout.DEFAULT);
    }

    /**
     * 显示Toast
     *
     * @param context 上下文
     * @param content 内容
     */
    public static void show(Context context, String content) {
        createToast(context, content, LENGTH_SHORT, ToastLayout.BOTTOM);
    }

    /**
     * 根据传入时间显示Toast
     *
     * @param context 上下文
     * @param content 内容
     * @param duration 时间
     */
    public static void show(Context context, String content, int duration) {
        createToast(context, content, duration, ToastLayout.DEFAULT);
    }

    /**
     * 根据传入位置显示Toast
     *
     * @param context 上下文
     * @param content 内容
     * @param layout 布局位置
     */
    public static void show(Context context, String content, ToastLayout layout) {
        createToast(context, content, LENGTH_SHORT, layout);
    }

    private static void createToast(Context context, String content, int duration, ToastLayout layout) {
        // 获取屏幕宽度
        Optional<Display> display = DisplayManager.getInstance().getDefaultDisplay(context);
        Point pt = new Point();
        display.get().getSize(pt);
        int width = (int) (display.get().getAttributes().width * WIDTH_RATIO); // 显示屏幕的百分之90

        DirectionalLayout.LayoutConfig textConfig = new DirectionalLayout.LayoutConfig(width,
            DirectionalLayout.LayoutConfig.MATCH_CONTENT);
        textConfig.setMargins(0, 0, 0, AttrHelper.vp2px(PADDING_SIZE_40, context));
        Text text = new Text(context);
        text.setMultipleLine(true);
        text.setText(content);
        text.setTextColor(new Color(Color.getIntColor("#000000")));
        text.setPadding(AttrHelper.vp2px(PADDING_SIZE_32, context),
            AttrHelper.vp2px(PADDING_SIZE_10, context),
            AttrHelper.vp2px(PADDING_SIZE_32, context),
            AttrHelper.vp2px(PADDING_SIZE_10, context));
        text.setTextSize(AttrHelper.fp2px(PADDING_SIZE_12, context));
        text.setBackground(buildDrawableByColorRadius(Color.getIntColor("#99EAE9E9"), vp2px(context, PADDING_SIZE_20)));
        text.setLayoutConfig(textConfig);
        DirectionalLayout toastLayout = new DirectionalLayout(context);
        toastLayout.addComponent(text);
        int mLayout = LayoutAlignment.CENTER;
        switch (layout) {
            case TOP:
                mLayout = LayoutAlignment.TOP;
                break;
            case BOTTOM:
                mLayout = LayoutAlignment.BOTTOM;
                break;
            case CENTER:
                mLayout = LayoutAlignment.CENTER;
                break;
            default:
                break;
        }
        ToastDialog toastDialog = new ToastDialog(context);
        toastDialog.setComponent(toastLayout);
        toastDialog.setSize(DirectionalLayout.LayoutConfig.MATCH_CONTENT, DirectionalLayout.LayoutConfig.MATCH_CONTENT);
        toastDialog.setAlignment(mLayout);
        toastDialog.setTransparent(true);
        toastDialog.setDuration(duration);
        toastDialog.show();
    }

    /**
     * 绘制背景颜色
     *
     * @param color 背景颜色
     * @param radius 半径
     * @return ohos.agp.components.element.Element 元素
     */
    private static ohos.agp.components.element.Element buildDrawableByColorRadius(int color, float radius) {
        ShapeElement drawable = new ShapeElement();
        drawable.setShape(0);
        drawable.setRgbColor(RgbColor.fromArgbInt(color));
        drawable.setCornerRadius(radius);
        return drawable;
    }

    private static String getString(Context context, int resId) {
        try {
            return context.getResourceManager().getElement(resId).getString();
        } catch (IOException e) {
            Log.error(TAG, "getString IOException: " + e.getLocalizedMessage());
        } catch (NotExistException e) {
            Log.error(TAG, "getString NotExistException: " + e.getLocalizedMessage());
        } catch (WrongTypeException e) {
            Log.error(TAG, "getString WrongTypeException: " + e.getLocalizedMessage());
        }
        return "";
    }

    private static int vp2px(Context context, float vp) {
        DisplayAttributes attributes = DisplayManager.getInstance().getDefaultDisplay(context).get().getAttributes();
        return (int) (attributes.densityPixels * vp);
    }
}