/*
 * Copyright (C) 2021 Huawei Device Co., Ltd.
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 */

package com.github.florent37.camerafragment.internal.ui.view;

import ohos.agp.components.AttrSet;
import ohos.agp.components.Component;
import ohos.agp.components.StackLayout;
import ohos.app.Context;

/**
 * 方向帧布局
 *
 * @since 2021-05-25
 */
public class AspectFrameLayout extends StackLayout implements Component.EstimateSizeListener {
    private static final double NEGATIVE = -1.0;
    private static final double CONSTANT_0_01 = 0.01;
    private double targetAspectRatio = NEGATIVE;

    /**
     * 构造函数
     *
     * @param context 上下文
     */
    public AspectFrameLayout(Context context) {
        this(context, null);
    }

    /**
     * 构造函数
     *
     * @param context 上下文
     * @param attrSet attr集合
     */
    public AspectFrameLayout(Context context, AttrSet attrSet) {
        this(context, attrSet, null);
    }

    /**
     * 构造函数
     *
     * @param context 上下文
     * @param attrSet attr集合
     * @param styleName 类型名字
     */
    public AspectFrameLayout(Context context, AttrSet attrSet, String styleName) {
        super(context, attrSet, styleName);
        setEstimateSizeListener(this);
    }

    /**
     * 设置宽高比
     *
     * @param aspectRatio 宽高比例
     */
    public void setAspectRatio(double aspectRatio) {
        if (aspectRatio < 0) {
            return;
        }

        if (targetAspectRatio != aspectRatio) {
            targetAspectRatio = aspectRatio;
            postLayout();
        }
    }

    @Override
    public int getWidth() {
        return super.getWidth();
    }

    @Override
    public int getHeight() {
        return super.getHeight();
    }

    @Override
    public boolean onEstimateSize(int ii, int i1) {
        if (targetAspectRatio > 0) {
            int width = ii;
            int height = i1;
            int initialWidth = EstimateSpec.getSize(width);
            int initialHeight = EstimateSpec.getSize(height);
            int horizontalPadding = getPaddingLeft() + getPaddingRight(); // padding
            int verticalPadding = getPaddingTop() + getPaddingBottom();
            initialWidth -= horizontalPadding;
            initialHeight -= verticalPadding;
            double viewAspectRatio = (double) initialWidth / initialHeight;
            double aspectDifference = targetAspectRatio / viewAspectRatio - 1;

            if (Math.abs(aspectDifference) > CONSTANT_0_01) {
                if (aspectDifference > 0) {
                    initialHeight = (int) (initialWidth / targetAspectRatio);
                } else {
                    initialWidth = (int) (initialHeight * targetAspectRatio);
                }
                initialWidth += horizontalPadding;
                initialHeight += verticalPadding;
                EstimateSpec.getSizeWithMode(initialWidth, EstimateSpec.PRECISE);
                EstimateSpec.getSizeWithMode(initialHeight, EstimateSpec.PRECISE);
            }
        }
        return true;
    }
}
