/*
 * Copyright (C) 2021 Huawei Device Co., Ltd.
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 */

package com.github.florent37.camerafragment.internal.timer;

import ohos.eventhandler.EventHandler;
import ohos.eventhandler.EventRunner;

/**
 * 时间任务基类
 *
 * @since 2021-05-25
 */
public abstract class TimerTaskBase {
    /**
     * 时间接口回调
     *
     * @since 2021-05-25
     */
    public interface Callback {
        /**
         * 设置时间文本
         *
         * @param text 时间字符串
         */
        void setText(String text);

        /**
         * 设置文本是否可见
         *
         * @param isVisible 是否可见
         */
        void setTextVisible(boolean isVisible);
    }

    /**
     * Handler
     */
    protected EventHandler handler = new EventHandler(EventRunner.getMainEventRunner());
    /**
     * 是否存活
     */
    protected boolean isAlive = false;
    /**
     * 记录时间秒数
     */
    protected long recordingTimeSeconds = 0L;
    /**
     * 记录时间分钟
     */
    protected long recordingTimeMinutes = 0L;

    /**
     * 结果回调
     */
    protected Callback callback;

    /**
     * 构造参数
     *
     * @param callback 时间回调
     */
    protected TimerTaskBase(Callback callback) {
        this.callback = callback;
    }

    /**
     * 停止计时
     */
    public abstract void stop();

    /**
     * 开始计时
     */
    public abstract void start();
}
