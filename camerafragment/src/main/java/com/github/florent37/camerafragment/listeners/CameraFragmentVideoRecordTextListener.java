/*
 * Copyright (C) 2021 Huawei Device Co., Ltd.
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 */

package com.github.florent37.camerafragment.listeners;

/**
 * 相机Fraction录像记录时间简监听
 *
 * @since 2021-05-25
 */
public interface CameraFragmentVideoRecordTextListener {
    /**
     * 设置录像大小文本
     *
     * @param size 大小
     * @param text 文本
     */
    void setRecordSizeText(long size, String text);

    /**
     * 设置录像文本是否可见
     *
     * @param isVisible 是否可见
     */
    void setRecordSizeTextVisible(boolean isVisible);

    /**
     * 设置录像时长文本
     *
     * @param text 时长文本
     */
    void setRecordDurationText(String text);

    /**
     * 设置录像时长文本是否可见
     *
     * @param isVisible
     */
    void setRecordDurationTextVisible(boolean isVisible);
}
