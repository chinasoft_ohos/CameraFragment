/*
 * Copyright (C) 2021 Huawei Device Co., Ltd.
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 */

package com.github.florent37.camerafragment.widgets;

import com.github.florent37.camerafragment.ResourceTable;
import com.github.florent37.camerafragment.internal.utils.Utils;

import ohos.agp.components.AttrHelper;
import ohos.agp.components.AttrSet;
import ohos.agp.components.Component;
import ohos.agp.components.Image;
import ohos.agp.components.element.ShapeElement;
import ohos.app.Context;
import ohos.media.image.PixelMap;
import ohos.multimodalinput.event.TouchEvent;

/**
 * 自定义前置后置切换view
 *
 * @since 2021-03-04
 */
public class CameraSwitchView extends Image implements Component.TouchEventListener, Component.ClickedListener {
    private static final int CONSTANT = 1000;
    private static final int PADDING = 5;
    private static final int WIDTH_CONSTANT = 2;
    private int padding = PADDING;
    private boolean isClick = false;
    private boolean isMove = false;
    private PixelMap frontCameraPixelMap;
    private PixelMap rearCameraPixelMap;

    private PixelMap frontCameraPixelMapYellow;
    private PixelMap rearCameraPixelMapYellow;

    private float downY;
    private float downX;
    private long clickTime;
    private long constantTime = CONSTANT;
    private CameraSwitchViewListener listener;

    /**
     * 构造方法
     *
     * @param context 上下文
     */
    public CameraSwitchView(Context context) {
        this(context, null);
    }

    /**
     * 构造方法
     *
     * @param context 上下文
     * @param attrSet 属性集
     */
    public CameraSwitchView(Context context, AttrSet attrSet) {
        this(context, attrSet, null);
    }

    /**
     * 构造方法
     *
     * @param context 上下文
     * @param attrSet 属性值
     * @param styleName 类型名字
     */
    public CameraSwitchView(Context context, AttrSet attrSet, String styleName) {
        super(context, attrSet, styleName);
        initializeView();
    }

    private void initializeView() {
        Context context = getContext();
        rearCameraPixelMap = Utils.getPixelMap(context, ResourceTable.Media_ic_camera_rear_white_24dp);
        frontCameraPixelMap = Utils.getPixelMap(context, ResourceTable.Media_ic_camera_front_white_24dp);

        rearCameraPixelMapYellow = Utils.getPixelMap(context, ResourceTable.Media_ic_camera_rear_yellow_24dp);
        frontCameraPixelMapYellow = Utils.getPixelMap(context, ResourceTable.Media_ic_camera_front_yellow_24dp);

        ShapeElement shapeElement = new ShapeElement(getContext(), ResourceTable.Graphic_circle_frame_background_dark);
        setBackground(shapeElement); // 设置背景颜色

        padding = AttrHelper.vp2px(padding, context);
        setPadding(padding, padding, padding, padding);
        displayBackCamera();
        setClickedListener(this);
        setTouchEventListener(this); // 点击切换图片效果
    }

    /**
     * 设置前置图片
     */
    public void displayFrontCamera() {
        setPixelMap(frontCameraPixelMap);
    }

    /**
     * 设置后置图片
     */
    public void displayBackCamera() {
        setPixelMap(rearCameraPixelMap);
    }

    /**
     * 触摸事件
     *
     * @param component 视图
     * @param touchEvent 触摸事件
     * @return boolean处理结果
     */
    @Override
    public boolean onTouchEvent(Component component, TouchEvent touchEvent) {
        switch (touchEvent.getAction()) {
            case TouchEvent.PRIMARY_POINT_DOWN: // 手指点下
                downX = touchEvent.getPointerScreenPosition(0).getX();
                downY = touchEvent.getPointerScreenPosition(0).getY();
                if (!isClick) {
                    setPixelMap(rearCameraPixelMapYellow);
                } else {
                    setPixelMap(frontCameraPixelMapYellow);
                }
                isMove = false;
                break;
            case TouchEvent.POINT_MOVE: // 手指在移动
                float moveX = touchEvent.getPointerScreenPosition(0).getX();
                float moveY = touchEvent.getPointerScreenPosition(0).getY();

                int width = getWidth() / WIDTH_CONSTANT;
                int height = getHeight() / WIDTH_CONSTANT;
                if (Math.abs(moveX - downX) > width || Math.abs(moveY - downY) > height) {
                    // 取消当前图片颜色,恢复白色底图
                    setPixelMap(!isClick ? rearCameraPixelMap : frontCameraPixelMap);
                    isMove = true;
                }
                break;
            case TouchEvent.PRIMARY_POINT_UP: // 手指抬起来
                if (!isMove) {
                    if (!isClick) {
                        setPixelMap(frontCameraPixelMap);
                        isClick = true;
                    } else {
                        setPixelMap(rearCameraPixelMap);
                        isClick = false;
                    }
                }
                break;
            default:
                break;
        }

        return true;
    }

    @Override
    public void onClick(Component component) {
        long nowTime = System.currentTimeMillis();
        if (listener != null) {
            long time = nowTime - clickTime;
            PixelMap pixelMap = getPixelMap();
            if (time > constantTime) {
                clickTime = nowTime;
                if (isClick) { // 判断当前哪张图片已经被点中
                    setPixelMap(frontCameraPixelMap);
                    isClick = true;
                } else {
                    setPixelMap(rearCameraPixelMap);
                    isClick = false;
                }
                listener.setSwitchListener();
            } else {
                isMove = true;
                if (pixelMap.toString().equals(rearCameraPixelMapYellow.toString())) { // 如果当前图片被选中就显示取消状态
                    setPixelMap(rearCameraPixelMap);
                } else {
                    setPixelMap(frontCameraPixelMap);
                }
            }
        }
    }

    /**
     * 接口回调
     *
     * @since 2021-04-19
     */
    public interface CameraSwitchViewListener {
        /**
         * 切换前置和后置
         */
        void setSwitchListener();
    }

    /**
     * 相机切换监听
     *
     * @param lis 相机切换监听
     */
    public void setCameraSwitchListener(CameraSwitchViewListener lis) {
        this.listener = lis;
    }
}
