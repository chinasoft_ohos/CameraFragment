/*
 * Copyright (C) 2021 Huawei Device Co., Ltd.
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 */

package com.github.florent37.camerafragment.internal.utils;

import ohos.media.image.Image;
import ohos.media.image.common.ImageFormat;

import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;

/**
 * 图片保存
 *
 * @since 2021-05-25
 */
public class ImageSaver implements Runnable {
    private static final String TAG = "ImageSaver";

    private Image image;
    private File file;
    private ImageSaverCallback imageSaverCallback;

    /**
     * 图片保存回调
     *
     * @since 2021-05-25
     */
    public interface ImageSaverCallback {
        /**
         * 完成
         *
         * @param bytes 图片字节数组
         */
        void onSuccessFinish(byte[] bytes);

        /**
         * 失败
         */
        void onError();
    }

    /**
     * 构造函数
     *
     * @param image Image对象
     * @param file 文件
     */
    public ImageSaver(Image image, File file) {
        this.image = image;
        this.file = file;
    }

    /**
     * 构造函数
     *
     * @param image Image对象
     * @param file 文件
     * @param imageSaverCallback 图片保存回调
     */
    public ImageSaver(Image image, File file, ImageSaverCallback imageSaverCallback) {
        this.image = image;
        this.file = file;
        this.imageSaverCallback = imageSaverCallback;
    }

    /**
     * 开始操作
     */
    @Override
    public void run() {
        FileOutputStream output = null;
        try {
            Image.Component component = image.getComponent(ImageFormat.ComponentType.JPEG);
            byte[] bytes = new byte[component.remaining()];
            component.read(bytes);
            output = new FileOutputStream(file);
            output.write(bytes); // 写图像数据
            if (imageSaverCallback != null) {
                imageSaverCallback.onSuccessFinish(bytes);
            }
        } catch (IOException error) {
            Log.error(TAG, "Can't save the image file." + error.getLocalizedMessage());
            if (imageSaverCallback != null) {
                imageSaverCallback.onError();
            }
        } catch (IllegalStateException err) {
            Log.error(TAG, "Can't read the image file." + err.getLocalizedMessage());
            if (imageSaverCallback != null) {
                imageSaverCallback.onError();
            }
        } finally {
            if (image != null) {
                image.release();
            }
            if (output != null) {
                try {
                    output.close(); // 关闭流
                } catch (IOException er) {
                    Log.error(TAG, "image release occur exception! " + er.getLocalizedMessage());
                }
            }
        }
    }
}
