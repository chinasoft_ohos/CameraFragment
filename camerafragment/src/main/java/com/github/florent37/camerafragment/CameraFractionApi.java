/*
 * Copyright (C) 2021 Huawei Device Co., Ltd.
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 */

package com.github.florent37.camerafragment;

import com.github.florent37.camerafragment.internal.ui.model.PhotoQualityOption;
import com.github.florent37.camerafragment.listeners.CameraFragmentControlsListener;
import com.github.florent37.camerafragment.listeners.CameraFragmentResultListener;
import com.github.florent37.camerafragment.listeners.CameraFragmentStateListener;
import com.github.florent37.camerafragment.listeners.CameraFragmentVideoRecordTextListener;

/**
 * Fraction操作API
 *
 * @since 2021-05-21
 */
public interface CameraFractionApi {
    /**
     * 拍照,录像
     *
     * @param resultListener 相机结果回调
     * @param directoryPath 文件路径
     * @param fileName 文件名
     */
    void takePhotoOrCaptureVideo(CameraFragmentResultListener resultListener, String directoryPath, String fileName);

    /**
     * 打开设置弹窗
     */
    void openSettingDialog();

    /**
     * 获取图片支持分辨率
     *
     * @return PhotoQualityOption 拍照质量选项
     */
    PhotoQualityOption[] getPhotoQualities();

    /**
     * 选择相机前后置类型
     */
    void switchCameraTypeFrontBack();

    /**
     * 选择拍照/录像
     */
    void switchActionPhotoVideo();

    /**
     * 闪光灯模式开关
     */
    void toggleFlashMode();

    /**
     * Fraction页面状态
     *
     * @param cameraFragmentStateListener 状态监听
     */
    void setStateListener(CameraFragmentStateListener cameraFragmentStateListener);

    /**
     * 录像时间文本监听
     *
     * @param cameraFragmentVideoRecordTextListener 录像时间监听
     */
    void setTextListener(CameraFragmentVideoRecordTextListener cameraFragmentVideoRecordTextListener);

    /**
     * Fraction页面控制监听
     *
     * @param cameraFragmentControlsListener 操控页面监听
     */
    void setControlsListener(CameraFragmentControlsListener cameraFragmentControlsListener);

    /**
     * 拍照,录像结果处理监听
     *
     * @param cameraFragmentResultListener 处理结果监听
     */
    void setResultListener(CameraFragmentResultListener cameraFragmentResultListener);
}
