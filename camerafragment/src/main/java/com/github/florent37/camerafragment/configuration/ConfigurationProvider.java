/*
 * Copyright (C) 2021 Huawei Device Co., Ltd.
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 */

package com.github.florent37.camerafragment.configuration;

/**
 * 配置提供者
 *
 * @since 2021-05-24
 */
public interface ConfigurationProvider {
    /**
     * 获取媒体动作
     *
     * @return int整数
     */
    int getMediaAction();

    /**
     * 获取照片画面质量
     *
     * @return int整数
     */
    int getMediaPhotoQuality();

    /**
     * 获取录像画面质量
     *
     * @return int整数
     */
    int getMediaVideoQuality();

    /**
     * 获取视频时长
     *
     * @return int整数
     */
    int getVideoDuration();

    /**
     * 获取视频文件大小
     *
     * @return long类型
     */
    long getVideoFileSize();

    /**
     * 获取传感器位置
     *
     * @return int整数
     */
    int getSensorPosition();

    /**
     * 获取旋转角度
     *
     * @return int整数
     */
    int getDegrees();

    /**
     * 获取录像最小时长
     *
     * @return int整数
     */
    int getMinimumVideoDuration();

    /**
     * 获取闪光灯模式
     *
     * @return int整数
     */
    int getFlashMode();

    /**
     * 获取相机设备
     *
     * @return int整数
     */
    int getCameraFace();

    /**
     * 设置图片质量
     *
     * @param mediaQuality 图片画质
     */
    void setMediaPhotoQuality(int mediaQuality);

    /**
     * 设置录像画面质量
     *
     * @param videoQuality 视频画质
     */
    void setMediaVideoQuality(int videoQuality);

    /**
     * 设置媒体质量
     *
     * @param mediaQuality
     */
    void setPassedMediaQuality(int mediaQuality);

    /**
     * 设置视频时长
     *
     * @param videoDuration 画质
     */
    void setVideoDuration(int videoDuration);

    /**
     * 设置视频文件大小
     *
     * @param videoFileSize 文件大小
     */
    void setVideoFileSize(long videoFileSize);

    /**
     * 设置视频最小时长
     *
     * @param minimumVideoDuration 时长
     */
    void setMinimumVideoDuration(int minimumVideoDuration);

    /**
     * 设置闪光灯模式
     *
     * @param flashMode 模式
     */
    void setFlashMode(int flashMode);

    /**
     * 设置传感器位置
     *
     * @param sensorPosition 传感器位置
     */
    void setSensorPosition(int sensorPosition);

    /**
     * 获取设备默认方向
     *
     * @return int整数
     */
    int getDeviceDefaultOrientation();

    /**
     * 设置旋转角度
     *
     * @param degrees 旋转角度
     */
    void setDegrees(int degrees);

    /**
     * 设置媒体操作
     *
     * @param mediaAction 媒体操作
     */
    void setMediaAction(int mediaAction);

    /**
     * 设置设备默认方向
     *
     * @param deviceDefaultOrientation 默认方向
     */
    void setDeviceDefaultOrientation(int deviceDefaultOrientation);

    /**
     * 获取媒体质量
     *
     * @return int整数
     */
    int getPassedMediaQuality();

    /**
     * 设置媒体配置项
     *
     * @param configuration 配置项
     */
    void setupWithAnnaConfiguration(Configuration configuration);
}
