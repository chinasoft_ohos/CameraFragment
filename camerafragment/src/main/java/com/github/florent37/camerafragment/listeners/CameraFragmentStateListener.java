/*
 * Copyright (C) 2021 Huawei Device Co., Ltd.
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 */

package com.github.florent37.camerafragment.listeners;

import java.io.File;

/**
 * 相机Fraction状态监听
 *
 * @since 2021-05-25
 */
public interface CameraFragmentStateListener {
    /**
     * 当前显示的摄像头是背面
     */
    void onCurrentCameraBack();

    /**
     * 当前显示的摄像头为前置时
     */
    void onCurrentCameraFront();

    /**
     * 当闪光灯处于自动模式时
     */
    void onFlashAuto();

    /**
     * 当闪光灯打开时
     */
    void onFlashOn();

    /**
     * 当闪光灯关闭时
     */
    void onFlashOff();

    /**
     * 如果相机准备好拍照
     */
    void onCameraSetupForPhoto();

    /**
     * 如果摄像机准备好拍摄视频
     */
    void onCameraSetupForVideo();

    /**
     * 当摄像机状态为“准备录像”时
     */
    void onRecordStateVideoReadyForRecord();

    /**
     * 当摄像机状态为“录像”时
     */
    void onRecordStateVideoInProgress();

    /**
     * 当相机状态为“准备拍照”时
     */
    void onRecordStatePhoto();

    /**
     * 屏幕/摄像机旋转后
     *
     * @param degrees 旋转角度
     */
    void shouldRotateControls(int degrees);

    /**
     * 开始录像
     *
     * @param outputFile 文件路径
     */
    void onStartVideoRecord(File outputFile);

    /**
     * 停止录像
     */
    void onStopVideoRecord();
}
