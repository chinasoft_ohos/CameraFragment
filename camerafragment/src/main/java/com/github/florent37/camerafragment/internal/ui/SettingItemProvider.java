/*
 * Copyright (C) 2021 Huawei Device Co., Ltd.
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 */

package com.github.florent37.camerafragment.internal.ui;

import com.github.florent37.camerafragment.ResourceTable;

import ohos.agp.components.BaseItemProvider;
import ohos.agp.components.Component;
import ohos.agp.components.ComponentContainer;
import ohos.agp.components.LayoutScatter;
import ohos.agp.components.Text;
import ohos.app.Context;

/**
 * 设置条目选择器
 *
 * @since 2021-03-10
 */
public class SettingItemProvider extends BaseItemProvider {
    private CharSequence[] charSequences;
    private Context context;
    private int optionCheckedIndex;
    private OnItemChangeListener onItemChangeListener; // 接口回调

    /**
     * 初始化设置构造参数
     *
     * @param charSequences 分辨率格式
     * @param context 上下文
     * @param optionCheckedIndex 选项索引
     */
    SettingItemProvider(CharSequence[] charSequences, Context context, int optionCheckedIndex) {
        this.charSequences = charSequences;
        this.context = context;
        this.optionCheckedIndex = optionCheckedIndex;
    }

    @Override
    public int getCount() {
        return charSequences == null ? 0 : charSequences.length;
    }

    @Override
    public CharSequence getItem(int ii) {
        return charSequences[ii];
    }

    @Override
    public long getItemId(int id) {
        return id;
    }

    @Override
    public Component getComponent(int position, Component component, ComponentContainer componentContainer) {
        ViewHolder viewHolder;
        Component component1 = component;
        if (null == component1) {
            LayoutScatter instance = LayoutScatter.getInstance(context);
            component1 = instance.parse(ResourceTable.Layout_item_setting_adapter, null, false);
            viewHolder = new ViewHolder(component1);
            componentContainer.setTag(viewHolder);
        } else {
            viewHolder = (ViewHolder) componentContainer.getTag();
        }

        if (optionCheckedIndex == position) {
            if (onItemChangeListener != null) {
                onItemChangeListener.setItemChange(component1);
            }
        }
        viewHolder.format.setText(charSequences[position].toString());
        return component1;
    }

    /**
     * ViewHolder
     *
     * @since 2021-04-20
     */
    public static class ViewHolder {
        private Text format;

        /**
         * ViewHolder
         *
         * @param component 视图Component
         */
        public ViewHolder(Component component) {
            format = (Text) component.findComponentById(ResourceTable.Id_format);
        }
    }

    /**
     * 条目位置改变监听
     *
     * @since 2021-04-20
     */
    public interface OnItemChangeListener {
        /**
         * 设置条目改变view
         *
         * @param component 视图
         */
        void setItemChange(Component component);
    }

    public void setOnItemChange(OnItemChangeListener itemChangeListener) {
        this.onItemChangeListener = itemChangeListener;
    }
}
