/*
 * Copyright (C) 2021 Huawei Device Co., Ltd.
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 */

package com.github.florent37.camerafragment.internal.manager.listener;

/**
 * 相机关闭接口
 *
 * @param <Integer>
 * @since 2021-05-24
 */
public interface CameraCloseListener<Integer> {
    /**
     * 关闭相机
     *
     * @param closedCameraId 关闭相机Id
     */
    void onCameraClosed(java.lang.Integer closedCameraId);
}
