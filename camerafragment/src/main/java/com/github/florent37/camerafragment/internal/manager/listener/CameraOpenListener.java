/*
 * Copyright (C) 2021 Huawei Device Co., Ltd.
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 */

package com.github.florent37.camerafragment.internal.manager.listener;

import com.github.florent37.camerafragment.internal.utils.Size;

/**
 * 相机打开接口
 *
 * @param <Integer>
 * @param <SurfaceListener>
 * @since 2021-05-24
 */
public interface CameraOpenListener<Integer, SurfaceListener> {
    /**
     * 打开相机
     *
     * @param openedCameraId 相机Id
     * @param previewSize 预览页面大小
     * @param surfaceListener 预览监听
     */
    void onCameraOpened(java.lang.Integer openedCameraId, Size previewSize, SurfaceListener surfaceListener);

    /**
     * 打开失败
     */
    void onCameraOpenError();
}
