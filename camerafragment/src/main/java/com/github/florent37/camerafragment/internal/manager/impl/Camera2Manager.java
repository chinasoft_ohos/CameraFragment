/*
 * Copyright (C) 2021 Huawei Device Co., Ltd.
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 */

package com.github.florent37.camerafragment.internal.manager.impl;

import static ohos.media.camera.device.Camera.FrameConfigType.FRAME_CONFIG_PICTURE;
import static ohos.media.camera.device.Camera.FrameConfigType.FRAME_CONFIG_PREVIEW;
import static ohos.media.camera.device.Camera.FrameConfigType.FRAME_CONFIG_RECORD;

import com.github.florent37.camerafragment.configuration.Configuration;
import com.github.florent37.camerafragment.configuration.ConfigurationProvider;
import com.github.florent37.camerafragment.internal.manager.listener.CameraCloseListener;
import com.github.florent37.camerafragment.internal.manager.listener.CameraOpenListener;
import com.github.florent37.camerafragment.internal.manager.listener.CameraPhotoListener;
import com.github.florent37.camerafragment.internal.manager.listener.CameraVideoListener;
import com.github.florent37.camerafragment.internal.ui.model.PhotoQualityOption;
import com.github.florent37.camerafragment.internal.ui.model.VideoQualityOption;
import com.github.florent37.camerafragment.internal.utils.CameraHelper;
import com.github.florent37.camerafragment.internal.utils.ImageSaver;
import com.github.florent37.camerafragment.internal.utils.Log;
import com.github.florent37.camerafragment.internal.utils.Size;
import com.github.florent37.camerafragment.listeners.CameraFragmentResultListener;

import ohos.aafwk.ability.fraction.FractionAbility;
import ohos.agp.components.AttrHelper;
import ohos.agp.components.ComponentContainer;
import ohos.agp.components.DirectionalLayout;
import ohos.agp.components.surfaceprovider.SurfaceProvider;
import ohos.agp.graphics.Surface;
import ohos.agp.graphics.SurfaceOps;
import ohos.agp.utils.Point;
import ohos.agp.window.service.Display;
import ohos.agp.window.service.DisplayManager;
import ohos.app.Context;
import ohos.media.camera.CameraKit;
import ohos.media.camera.device.Camera;
import ohos.media.camera.device.CameraAbility;
import ohos.media.camera.device.CameraConfig;
import ohos.media.camera.device.CameraInfo;
import ohos.media.camera.device.CameraStateCallback;
import ohos.media.camera.device.FrameConfig;
import ohos.media.camera.device.FrameResult;
import ohos.media.camera.device.FrameStateCallback;
import ohos.media.camera.params.AfResult;
import ohos.media.camera.params.Metadata;
import ohos.media.camera.params.ParameterKey;
import ohos.media.common.AudioProperty;
import ohos.media.common.Source;
import ohos.media.common.StorageProperty;
import ohos.media.common.VideoProperty;
import ohos.media.image.Image;
import ohos.media.image.ImageReceiver;
import ohos.media.image.common.ImageFormat;
import ohos.media.recorder.Recorder;
import ohos.media.recorder.RecorderProfile;
import ohos.sensor.bean.CategoryOrientation;

import java.io.File;
import java.text.DecimalFormat;
import java.util.ArrayList;
import java.util.List;
import java.util.Objects;
import java.util.Optional;
import java.util.UUID;

/**
 * 相机管理2
 *
 * @since 2021-05-25
 */
public class Camera2Manager extends BaseCameraManager<Integer, SurfaceOps.Callback> {
    private static final String TAG = "Camera2Manager";
    private static final double CONSTANT_0_6 = 0.6;
    private static final double CONSTANT_06 = 0.6;
    private static final double CONSTANT_0_7 = 0.7;
    private static final double CONSTANT_0_9 = 0.9;
    private static final double CONSTANT_1_1 = 1.1;
    private static final double CONSTANT_1_2 = 1.2;
    private static final double CONSTANT_1_3 = 1.3;
    private static final double CONSTANT_1_35 = 1.35;
    private static final double CONSTANT_1_5 = 1.5;
    private static final double CONSTANT_1_6 = 1.6;
    private static final double CONSTANT_3_4 = 3.4;
    private static final double CONSTANT_3_5 = 3.5;
    private static final double CONSTANT_6 = 6;
    private static final double CONSTANT_8 = 8;
    private static final double CONSTANT_10 = 10;
    private static final int CONSTANT_0 = 0;
    private static final int CONSTANT_1 = 1;
    private static final int CONSTANT_2 = 2;
    private static final int CONSTANT_3 = 3;
    private static final int CONSTANT_5 = 5;
    private static final int DEGREE_0 = 0;
    private static final int DEGREE_180 = 180;
    private static final int ROTATE_90 = 90;
    private static final int ROTATE_180 = 180;
    private static final int ROTATE_270 = 270;
    private static final int ROTATE_360 = 360;
    private static final int CONSTANT_600 = 600;
    private static final int BIT_RATE = 10000000;
    private static final int FPS = 30;
    private static final int AUDIO_NUM_CHANNELS_STEREO = 2;
    private static final int AUDIO_SAMPLE_RATE_HZ = 8000;
    private CameraPhotoListener cameraPhotoListener;
    private CameraVideoListener cameraVideoListener;
    private File outputPath;
    private int mediaAction;
    private int screenRotation;
    private int futurFlashMode = 0;
    private int displayRotation = 0;
    private Camera cameraCallback;
    private CameraKit cameraKit;
    private ImageReceiver imageReader;
    private CameraAbility frontCameraAbility;
    private CameraAbility backCameraAbility;
    private Surface surface;
    private Surface recevingSurface;
    private String cameraIdString;
    private FrameConfig previewFrameConfig;
    private FrameConfig framePictureConfig;
    private FrameConfig previewRecordConfig;
    private FrameConfig.Builder frameConfigBuilder;
    private FrameConfig.Builder previewRequestBuilder;
    private FrameConfig.Builder framePictureConfigBuilder;
    private VideoProperty.Builder videoPropertyBuilder;
    private AudioProperty.Builder audioPropertyBuilder;
    private StorageProperty.Builder storagePropertyBuilder;
    private Source source;
    private CameraFragmentResultListener callback;
    private CameraStateCallbackImpl cameraStateCallback;
    private DirectionalLayout dependentLayout;
    private FractionAbility fractionAbility;
    private SurfaceProvider surfaceProvider;
    private double tempWidth;
    private double tempHeight;

    @Override
    public void initializeCameraManager(ConfigurationProvider configurationProvider, Context context) {
        super.initializeCameraManager(configurationProvider, context);
        Display display = DisplayManager.getInstance().getDefaultDisplay(context).get();
        Point size = new Point();
        display.getSize(size);
        windowSize = new Size(size.getPointXToInt(), size.getPointYToInt());
        cameraKit = CameraKit.getInstance(context);
        if (cameraKit == null) {
            return;
        }
        String[] cameraIds = cameraKit.getCameraIds(); // 获取当前设备的逻辑相机列表
        if (cameraIds.length <= 0) {
            Log.error(TAG, "cameraIds size is 0");
            return;
        }
        cameraIdString = cameraIds[0];
        numberOfCameras = cameraIds.length;
        for (int ii = 0; ii < cameraIds.length; ii++) {
            CameraInfo cameraInfo = cameraKit.getCameraInfo(cameraIds[ii]);
            CameraAbility cameraAbility = cameraKit.getCameraAbility(cameraIds[ii]);
            int facingType = cameraInfo.getFacingType(); // 获取相机正面类型
            if (facingType == CameraInfo.FacingType.CAMERA_FACING_FRONT) { // 前置摄像头
                faceFrontCameraId = ii;
                faceFrontCameraOrientation = cameraInfo.getDeviceLinkType(
                    CategoryOrientation.STRING_SENSOR_TYPE_DEVICE_ORIENTATION);
                frontCameraAbility = cameraAbility;
            } else if (facingType == CameraInfo.FacingType.CAMERA_FACING_BACK) { // 后置摄像头
                faceBackCameraId = ii;
                faceBackCameraOrientation = cameraInfo.getDeviceLinkType(
                    CategoryOrientation.STRING_SENSOR_TYPE_DEVICE_ORIENTATION);
                backCameraAbility = cameraAbility;
            }
        }
    }

    // 初始化SurfaceView
    @Override
    public void initSurfaceComponent(DirectionalLayout layout, FractionAbility fraction,
                                     CameraOpenListener<Integer, SurfaceOps.Callback> listener) {
        if (cameraIdString != null) {
            this.dependentLayout = layout;
            this.fractionAbility = fraction;
            surfaceProvider = new SurfaceProvider(context);
            DirectionalLayout.LayoutConfig params = new DirectionalLayout.LayoutConfig(
                ComponentContainer.LayoutConfig.MATCH_PARENT, ComponentContainer.LayoutConfig.MATCH_PARENT);
            surfaceProvider.setLayoutConfig(params);
            surfaceProvider.pinToZTop(false);
            fraction.getWindow().setTransparent(true);
            prepareCameraOutputs(cameraIdString); // 准备相机图片输出格式
            prepareVideoRecorder(); // 为录像做初始化准备
            surfaceProvider.getSurfaceOps().get().addCallback(new SurfaceCallback());
            dependentLayout.addComponent(surfaceProvider);
        } else {
            if (listener != null) {
                listener.onCameraOpenError();
            }
        }
    }

    @Override
    public void openCamera(Context cameraContext, int currentCameraId,
                           CameraOpenListener<Integer, SurfaceOps.Callback> openListener) {
        this.mCurrentCameraId = currentCameraId;
        prepareCameraOutputs(String.valueOf(currentCameraId));
        if (futurFlashMode != 0) {
            setFlashMode(futurFlashMode);
            futurFlashMode = 0;
        }
        cameraKit.createCamera(String.valueOf(currentCameraId), cameraStateCallback, uiHandler); // 创建相机
        setSurfaceSize();
        if (openListener != null) {
            openListener.onCameraOpened(currentCameraId, previewSize,
                new OpenCameraSurfaceCallback());
        }
    }

    /**
     * 设置Surface大小
     */
    private void setSurfaceSize() {
        if (dependentLayout != null) {
            Size compare = compare(previewSize.getWidth(), previewSize.getHeight());
            DirectionalLayout.LayoutConfig params = new DirectionalLayout.LayoutConfig();
            params.width = compare.getWidth();
            params.height = compare.getHeight();
            surfaceProvider.setLayoutConfig(params);
            surfaceProvider.postLayout();
        }
    }

    @Override
    public void closeCamera(CameraCloseListener cameraCloseListener) {
        closeCamera();
        if (cameraCloseListener != null) {
            cameraCloseListener.onCameraClosed(mCurrentCameraId);
        }
    }

    private void closeCamera() {
        if (cameraCallback != null) {
            cameraCallback.release(); // 关闭相机和释放资源
            cameraCallback = null;
        }
        framePictureConfigBuilder = null; // 拍照配置模板置空
        previewRequestBuilder = null; // 录像配置模板置空
        previewFrameConfig = null; // 预览配置模板置空
    }

    @Override
    public void setFlashMode(int flashMode) {
        futurFlashMode = flashMode;
        setFlashModeAndBuildPreviewRequest(flashMode);
    }

    @Override
    public void takePhoto(File photoFile, CameraPhotoListener photoListener,
                          CameraFragmentResultListener listener) {
        this.outputPath = photoFile;
        this.cameraPhotoListener = photoListener;
        this.callback = listener;

        backgroundHandler.postTask(new Runnable() {
            @Override
            public void run() {
                capture();
            }
        });
    }

    @Override
    public Size getPhotoSizeForQuality(int mediaQuality) {
        final CameraAbility map = mCurrentCameraId.equals(faceBackCameraId) ? backCameraAbility : frontCameraAbility;
        return CameraHelper.getPictureSize(Size.fromArray2(
            changeArray(map.getSupportedSizes(ImageFormat.JPEG))), mediaQuality);
    }

    /**
     * 开始录制视频
     *
     * @param videoFile 录像文件
     * @param videoListener 录像监听
     */
    @Override
    public void startVideoRecord(File videoFile, CameraVideoListener videoListener) {
        if (isVideoRecording) {
            return;
        }
        this.outputPath = videoFile;
        this.cameraVideoListener = videoListener;
        if (videoListener != null) {
            cameraRecord();
            videoRecorder.start();
            isVideoRecording = true;
            videoListener.onVideoRecordStarted(videoSize);
        }
    }

    /**
     * 停止录制视频
     *
     * @param listener Fraction处理结果回调
     */
    @Override
    public void stopVideoRecord(CameraFragmentResultListener listener) {
        if (isVideoRecording) {
            releaseVideoRecorder();
            closeCamera();
            prepareVideoRecorder();
            isVideoRecording = false;
            if (cameraVideoListener != null) {
                cameraKit.createCamera(mCurrentCameraId.toString(), cameraStateCallback, uiHandler);
                cameraVideoListener.onVideoRecordStopped(outputPath, listener);
            }
        }
    }

    private void startPreview() {
        CameraAbility cameraAbility = cameraKit.getCameraAbility(cameraIdString);
        int cameraRotationOffset = DisplayManager.getInstance().getDefaultDisplay(context).get().getRotation();

        if (frameConfigBuilder == null && cameraCallback != null) {
            frameConfigBuilder = cameraCallback.getFrameConfigBuilder(FRAME_CONFIG_PREVIEW);
        }
        setAutoFocus(cameraAbility, frameConfigBuilder);
        setFlashMode(configurationProvider.getFlashMode());

        if (configurationProvider.getMediaAction() == Configuration.MEDIA_ACTION_PHOTO
            || configurationProvider.getMediaAction() == Configuration.MEDIA_ACTION_UNSPECIFIED) {
            setAutoFocus(cameraAbility, frameConfigBuilder);
        } else if (configurationProvider.getMediaAction() == Configuration.MEDIA_ACTION_PHOTO) {
            setAutoFocus(cameraAbility, frameConfigBuilder);
        }
        int rotation = DisplayManager.getInstance().getDefaultDisplay(context).get().getRotation();
        int degrees = 0;
        switch (rotation) {
            case CONSTANT_0:
                degrees = 0;
                break; // Natural orientation
            case CONSTANT_1:
                degrees = ROTATE_90;
                break; // Landscape left
            case CONSTANT_2:
                degrees = ROTATE_180;
                break; // Upside down
            case CONSTANT_3:
                degrees = ROTATE_270;
                break; // Landscape right
            default:
                break;
        }
        CameraInfo cameraInfo = cameraKit.getCameraInfo(cameraIdString);
        int facingType = cameraInfo.getFacingType(); // 获取相机正面类型
        if (facingType == CameraInfo.FacingType.CAMERA_FACING_FRONT) { // 前置
            displayRotation = (cameraRotationOffset + degrees) % ROTATE_360;
            displayRotation = (ROTATE_360 - displayRotation) % ROTATE_360; // compensate
        } else { // 后置
            displayRotation = (cameraRotationOffset - degrees + ROTATE_360) % ROTATE_360;
        }
    }

    // 设置自动对焦
    private void setAutoFocus(CameraAbility cameraAbility, FrameConfig.Builder parameters) {
        for (int ii : cameraAbility.getSupportedAfMode()) {
            if (cameraAbility.getSupportedAfMode()[ii] == AfResult.State.AF_STATE_AUTO_FOCUSED) {
                parameters.setAfMode(AfResult.State.AF_STATE_AUTO_FOCUSED, null);
                break;
            }
        }
    }

    /**
     * 最大持续时间
     */
    @Override
    protected void onMaxDurationReached() {
        stopVideoRecord(callback);
    }

    @Override
    protected void onMaxFileSizeReached() {
        stopVideoRecord(callback);
    }

    @Override
    protected int getPhotoOrientation(int sensorPosition) {
        return getVideoOrientation(sensorPosition);
    }

    @Override
    protected int getVideoOrientation(int sensorPosition) {
        final int degrees;
        switch (sensorPosition) {
            case Configuration.SENSOR_POSITION_LEFT:
                degrees = ROTATE_90;
                break; // Landscape left
            case Configuration.SENSOR_POSITION_UP_SIDE_DOWN:
                degrees = ROTATE_180;
                break; // Upside down
            case Configuration.SENSOR_POSITION_RIGHT:
                degrees = ROTATE_270;
                break; // Landscape right
            case Configuration.SENSOR_POSITION_UNSPECIFIED:
            case Configuration.SENSOR_POSITION_UP:
            default:
                degrees = 0;
                break;
        }
        final int rotate;
        if (Objects.equals(mCurrentCameraId, faceFrontCameraId)) {
            rotate = (ROTATE_360 + faceFrontCameraOrientation + degrees) % ROTATE_360;
        } else {
            rotate = (ROTATE_360 + faceBackCameraOrientation - degrees) % ROTATE_360;
        }
        return rotate;
    }

    private void closeImageReader() {
        if (imageReader != null) {
            imageReader.release();
            imageReader = null;
        }
    }

    @Override
    protected void prepareCameraOutputs(String camera) {
        if (mCurrentCameraId.equals(faceFrontCameraId)) {
            if (frontCameraAbility == null) {
                frontCameraAbility = CameraKit.getInstance(context).getCameraAbility(camera);
            }
        } else {
            if (backCameraAbility == null) {
                backCameraAbility = CameraKit.getInstance(context).getCameraAbility(camera);
            }
        }
        CameraAbility map = mCurrentCameraId.equals(faceBackCameraId) ? backCameraAbility : frontCameraAbility;
        if (configurationProvider.getMediaPhotoQuality() == Configuration.MEDIA_QUALITY_AUTO) {
            camcorderProfile = CameraHelper.getCamcorderProfile(camera,
                configurationProvider.getVideoFileSize(), configurationProvider.getMinimumVideoDuration());
        } else {
            camcorderProfile = CameraHelper.getCamcorderProfile(configurationProvider.getMediaPhotoQuality(), camera);
        }
        videoSize = CameraHelper.chooseOptimalSize(Size.fromArray2(changeArray(map.getSupportedSizes(Recorder.class))),
            windowSize.getWidth(), windowSize.getHeight(),
            new Size(camcorderProfile.vFrameWidth, camcorderProfile.vFrameHeight));

        if (videoSize == null) {
            Size[] sizes = Size.fromArray2(changeArray(map.getSupportedSizes(Recorder.class)));
            videoSize = CameraHelper.getSizeWithClosestRatio(sizes,
                camcorderProfile.vFrameWidth, camcorderProfile.vFrameHeight);
        } else if (videoSize.getWidth() > camcorderProfile.vFrameWidth
            || videoSize.getHeight() > camcorderProfile.vFrameHeight) {
            Size[] sizes = Size.fromArray2(changeArray(map.getSupportedSizes(Recorder.class)));
            videoSize = CameraHelper.getSizeWithClosestRatio(sizes,
                camcorderProfile.vFrameWidth, camcorderProfile.vFrameHeight);
        }
        Size[] sizes = Size.fromArray2(changeArray(map.getSupportedSizes(ImageFormat.JPEG)));
        photoSize = CameraHelper.getPictureSize(sizes,
            configurationProvider.getMediaPhotoQuality() == Configuration.MEDIA_QUALITY_AUTO
                ? Configuration.MEDIA_QUALITY_HIGHEST : configurationProvider.getMediaPhotoQuality());
        takePictureInit();
        getPreviewSize(map);
    }

    private void getPreviewSize(CameraAbility map) {
        if (configurationProvider.getMediaAction() == Configuration.MEDIA_ACTION_PHOTO
            || configurationProvider.getMediaAction() == Configuration.MEDIA_ACTION_UNSPECIFIED) {
            Size[] sizes = Size.fromArray2(changeArray(map.getSupportedSizes(SurfaceOps.class)));
            if (windowSize.getHeight() * windowSize.getWidth() > photoSize.getWidth() * photoSize.getHeight()) {
                // 把SurfaceTexture 改成 SurfaceOps
                previewSize = CameraHelper.getOptimalPreviewSize(sizes,
                    photoSize.getWidth(), photoSize.getHeight());
            } else {
                previewSize = CameraHelper.getOptimalPreviewSize(sizes,
                    windowSize.getWidth(), windowSize.getHeight());
            }

            if (previewSize == null) {
                Size[] sizes1 = Size.fromArray2(changeArray(map.getSupportedSizes(SurfaceOps.class)));
                previewSize = CameraHelper.chooseOptimalSize(sizes1,
                    windowSize.getWidth(), windowSize.getHeight(), photoSize);
            }
        } else {
            Size[] sizes = Size.fromArray2(changeArray(map.getSupportedSizes(SurfaceOps.class)));
            if (windowSize.getHeight() * windowSize.getWidth() > videoSize.getWidth() * videoSize.getHeight()) {
                previewSize = CameraHelper.getOptimalPreviewSize(sizes,
                    videoSize.getWidth(), videoSize.getHeight());
            } else {
                previewSize = CameraHelper.getOptimalPreviewSize(sizes,
                    windowSize.getWidth(), windowSize.getHeight());
            }

            if (previewSize == null) {
                Size[] sizes1 = Size.fromArray2(changeArray(map.getSupportedSizes(SurfaceOps.class)));
                previewSize = CameraHelper.getSizeWithClosestRatio(sizes1,
                    videoSize.getWidth(), videoSize.getHeight());
            }
        }
    }

    private void takePictureInit() {
        imageReader = ImageReceiver.create(photoSize.getWidth(), photoSize.getHeight(), ImageFormat.JPEG, CONSTANT_5);
        recevingSurface = imageReader.getRecevingSurface();
        ImageReceiverListener imageArrivalListener = new ImageReceiverListener();
        imageReader.setImageArrivalListener(imageArrivalListener);
    }

    // 将获取得到的List<Size> 转成数组Size[]
    private ohos.media.image.common.Size[] changeArray(List<ohos.media.image.common.Size> sizes) {
        ohos.media.image.common.Size[] sizes1 = new ohos.media.image.common.Size[sizes.size()];
        for (int ii = 0; ii < sizes.size(); ii++) {
            ohos.media.image.common.Size commonSize = new ohos.media.image.common.Size();
            commonSize.width = sizes.get(ii).width;
            commonSize.height = sizes.get(ii).height;
            sizes1[ii] = commonSize;
        }
        return sizes1;
    }

    /**
     * 准备录制视频
     */
    @Override
    public void prepareVideoRecorder() {
        context = context == null ? fractionAbility : context;
        source = new Source();
        videoRecorder = new Recorder();
        videoPropertyBuilder = new VideoProperty.Builder();
        audioPropertyBuilder = new AudioProperty.Builder();
        storagePropertyBuilder = new StorageProperty.Builder();
        videoPropertyBuilder.setRecorderBitRate(BIT_RATE); // 设置录制比特率
        if (mCurrentCameraId.equals(faceFrontCameraId)) { // 前置摄像头
            if (screenRotation == 0) { // 只判断竖屏
                videoPropertyBuilder.setRecorderDegrees(getOrientation(CONSTANT_3)); // 设置录像方向
            }
        } else { // 后置摄像头
            if (screenRotation == DEGREE_0 || screenRotation == DEGREE_180) { // 判断旋转角度是否为0 或180
                videoPropertyBuilder.setRecorderDegrees(getOrientation(CONSTANT_1)); // 设置录像方向为旋转0
            } else {
                videoPropertyBuilder.setRecorderDegrees(getOrientation(CONSTANT_0)); // 设置录像方向为旋转0
            }
        }
        videoPropertyBuilder.setRecorderFps(FPS); // 设置录制采样率
        videoPropertyBuilder.setRecorderRate(FPS); // 设置录制帧率
        videoPropertyBuilder.setRecorderHeight(Math.min(previewSize.getHeight(),
            previewSize.getWidth())); // 设置录像支持的分辨率，需保证width > height
        videoPropertyBuilder.setRecorderWidth(Math.max(previewSize.getHeight(),
            previewSize.getWidth()));
        videoPropertyBuilder.setRecorderVideoEncoder(Recorder.VideoEncoder.H264); // 设置视频编码方式
        source.setRecorderAudioSource(Recorder.AudioSource.MIC); // 设置录制音频源
        source.setRecorderVideoSource(Recorder.VideoSource.SURFACE); // 设置视频窗口
        videoRecorder.setSource(source); // 设置音视频源
        videoRecorder.setOutputFormat(Recorder.OutputFormat.MPEG_4); // 设置音视频输出格式

        StringBuffer fileName = new StringBuffer("VID_"); // 生成随机文件名
        fileName.append(UUID.randomUUID()).append(".mp4");
        File file = new File(context.getExternalFilesDir("").getAbsolutePath(), fileName.toString()); // 创建录像文件对象
        storagePropertyBuilder.setRecorderFile(file); // 设置存储音视频文件名
        audioPropertyBuilder.setRecorderAudioEncoder(Recorder.AudioEncoder.AAC); // 设置音频编码格式
        audioPropertyBuilder.setRecorderNumChannels(AUDIO_NUM_CHANNELS_STEREO);
        audioPropertyBuilder.setRecorderSamplingRate(AUDIO_SAMPLE_RATE_HZ);
        videoRecorder.setStorageProperty(storagePropertyBuilder.build()); // 设置存储属性
        videoRecorder.setAudioProperty(audioPropertyBuilder.build()); // 设置音频属性
        videoRecorder.setVideoProperty(videoPropertyBuilder.build()); // 设置视频属性
        videoRecorder.prepare();
    }

    @Override
    public void setScreenRotation(int degrees) {
        screenRotation = degrees;
    }

    @Override
    public void releaseCameraResource() {
        closeCamera();
    }

    private void updatePreview() {
        if (cameraCallback == null) {
            return;
        }
        setFlashModeAndBuildPreviewRequest(configurationProvider.getFlashMode());
    }

    // 拍照
    private void capture() {
        try {
            if (frameConfigBuilder == null) {
                // 获取拍照配置模板
                framePictureConfigBuilder = cameraCallback.getFrameConfigBuilder(FRAME_CONFIG_PICTURE);
                framePictureConfigBuilder.setFlashMode(Metadata.FlashMode.FLASH_ALWAYS_OPEN);
            } else {
                framePictureConfigBuilder = frameConfigBuilder;
            }

            framePictureConfigBuilder.addSurface(imageReader.getRecevingSurface()); // 配置拍照Surface

            if (mCurrentCameraId.equals(faceFrontCameraId)) { // 判断是否为前置,是前置就旋转270
                framePictureConfigBuilder.setParameter(ParameterKey.IMAGE_MIRROR, true); // 设置镜像属性
                framePictureConfigBuilder.setImageRotation(ROTATE_270);
            } else { // 否则就是后置旋转90度
                framePictureConfigBuilder.setImageRotation(ROTATE_90); // 拍照时需要加上90角旋转,不然拍出来就图片会逆向旋转90度
            }
            framePictureConfig = framePictureConfigBuilder.build();
            cameraCallback.triggerSingleCapture(framePictureConfig); // 启动单帧捕获(拍照)
        } catch (IllegalArgumentException e) {
            Log.error(TAG, "capture IllegalArgumentException : " + e.getLocalizedMessage());
        } catch (IllegalStateException e) {
            Log.error(TAG, "capture IllegalStateException :" + e.getLocalizedMessage());
        }
    }

    // 录像配置模板
    private void cameraRecord() {
        previewRequestBuilder = cameraCallback.getFrameConfigBuilder(FRAME_CONFIG_RECORD); // 获取录像配置模板
        previewRequestBuilder.addSurface(surface); // 配置预览Surface
        Surface mRecorderSurface = videoRecorder.getVideoSurface();
        previewRequestBuilder.addSurface(mRecorderSurface);
        previewRecordConfig = previewRequestBuilder.build();
        try {
            // 启动循环帧捕获  通过camera.stopLoopingCapture()方法停止循环帧捕获（录像）。
            cameraCallback.triggerLoopingCapture(previewRecordConfig);
        } catch (IllegalArgumentException error) {
            Log.error(TAG, "cameraRecordArgumentException " + error.getLocalizedMessage());
        } catch (IllegalStateException error) {
            Log.error(TAG, "cameraRecordStateException " + error.getLocalizedMessage());
        }
    }

    private void lockFocus() {
        previewRequestBuilder.setParameter(Configuration.CONTROL_AF_TRIGGER, Metadata.AfTrigger.AF_TRIGGER_START);
    }

    private void runPreCaptureSequence() {
        previewRequestBuilder.setParameter(Configuration.CONTROL_AE_PRECAPTURE_TRIGGER,
            Metadata.AfTrigger.AF_TRIGGER_START);
    }

    private void unlockFocus() {
        previewRequestBuilder.setParameter(Configuration.CONTROL_AF_TRIGGER,
            Metadata.AfTrigger.AF_TRIGGER_CANCEL);
    }

    private void setFlashModeAndBuildPreviewRequest(@Configuration.FlashMode int flashMode) {
        if (framePictureConfigBuilder == null) {
            framePictureConfigBuilder = cameraCallback.getFrameConfigBuilder(FRAME_CONFIG_PICTURE);
        }
        switch (flashMode) {
            case Configuration.FLASH_MODE_ON:
                framePictureConfigBuilder.setFlashMode(Metadata.FlashMode.FLASH_ALWAYS_OPEN);
                break;
            case Configuration.FLASH_MODE_OFF:
                framePictureConfigBuilder.setFlashMode(Metadata.FlashMode.FLASH_CLOSE);
                break;
            case Configuration.FLASH_MODE_AUTO:
            default:
                framePictureConfigBuilder.setFlashMode(Metadata.FlashMode.FLASH_AUTO);
                break;
        }
        framePictureConfig = framePictureConfigBuilder.build();
    }

    // 设置录制方向
    private int getOrientation(int rotation) {
        int orientation = 0;
        switch (rotation) {
            case 0: // 默认竖屏
                orientation = Recorder.OrientationHint.FIRST_PLAYBACK_DERGREE;
                break;
            case CONSTANT_1: // 旋转90
                orientation = Recorder.OrientationHint.SECOND_PLAYBACK_DERGREE;
                break;
            case CONSTANT_2: // 旋转180
                orientation = Recorder.OrientationHint.THIRD_PLAYBACK_DERGREE;
                break;
            case CONSTANT_3: // 旋转270
                orientation = Recorder.OrientationHint.FOURTH_PLAYBACK_DERGREE;
                break;
            default:
                break;
        }

        return orientation;
    }

    /**
     * 单帧捕获生成图像回调Listener
     *
     * @since 2021-05-25
     */
    private class ImageReceiverListener implements ImageReceiver.IImageArrivalListener, ImageSaver.ImageSaverCallback {
        @Override
        public void onImageArrival(ImageReceiver imageReceiver) {
            Image image = imageReceiver.readNextImage();
            final File outputFile = outputPath;
            backgroundHandler.postTask(new ImageSaver(image, outputFile, this));
        }

        @Override
        public void onSuccessFinish(byte[] bytes) {
            if (cameraPhotoListener != null) {
                uiHandler.postTask(new Runnable() {
                    @Override
                    public void run() {
                        cameraPhotoListener.onPhotoTaken(outputPath, callback);
                        callback = null;
                    }
                });
            }
            unlockFocus();
        }

        @Override
        public void onError() {
            Log.error(TAG, "onPhotoError: ");
            uiHandler.postTask(new Runnable() {
                @Override
                public void run() {
                    cameraPhotoListener.onPhotoTakeError();
                }
            });
        }
    }

    @Override
    public void setMediaAction(int mediaAction) {
        this.mediaAction = mediaAction;
    }

    @Override
    public CharSequence[] getVideoQualityOptions() {
        final List<CharSequence> videoQualities = new ArrayList<>();

        if (configurationProvider.getMinimumVideoDuration() > 0) {
            videoQualities.add(new VideoQualityOption(Configuration.MEDIA_QUALITY_AUTO,
                CameraHelper.getCamcorderProfile(Configuration.MEDIA_QUALITY_AUTO,
                    getCurrentCameraId()), configurationProvider.getMinimumVideoDuration()));
        }
        RecorderProfile camcorderProfile = CameraHelper.getCamcorderProfile(
            Configuration.MEDIA_QUALITY_HIGH, mCurrentCameraId);
        double videoDuration = CameraHelper.calculateApproximateVideoDuration(camcorderProfile,
            configurationProvider.getVideoFileSize());
        videoQualities.add(new VideoQualityOption(Configuration.MEDIA_QUALITY_HIGH,
            camcorderProfile, videoDuration));

        camcorderProfile = CameraHelper.getCamcorderProfile(Configuration.MEDIA_QUALITY_MEDIUM, mCurrentCameraId);
        videoDuration = CameraHelper.calculateApproximateVideoDuration(camcorderProfile,
            configurationProvider.getVideoFileSize());
        videoQualities.add(new VideoQualityOption(Configuration.MEDIA_QUALITY_MEDIUM, camcorderProfile, videoDuration));

        camcorderProfile = CameraHelper.getCamcorderProfile(Configuration.MEDIA_QUALITY_LOW, mCurrentCameraId);
        videoDuration = CameraHelper.calculateApproximateVideoDuration(camcorderProfile,
            configurationProvider.getVideoFileSize());
        videoQualities.add(new VideoQualityOption(Configuration.MEDIA_QUALITY_LOW, camcorderProfile, videoDuration));

        CharSequence[] array = new CharSequence[videoQualities.size()];
        videoQualities.toArray(array);

        return array;
    }

    @Override
    public CharSequence[] getPhotoQualityOptions() {
        final List<CharSequence> photoQualities = new ArrayList<>();
        photoQualities.add(new PhotoQualityOption(Configuration.MEDIA_QUALITY_HIGHEST,
            getPhotoSizeForQuality(Configuration.MEDIA_QUALITY_HIGHEST)));
        photoQualities.add(new PhotoQualityOption(Configuration.MEDIA_QUALITY_HIGH,
            getPhotoSizeForQuality(Configuration.MEDIA_QUALITY_HIGH)));
        photoQualities.add(new PhotoQualityOption(Configuration.MEDIA_QUALITY_MEDIUM,
            getPhotoSizeForQuality(Configuration.MEDIA_QUALITY_MEDIUM)));
        photoQualities.add(new PhotoQualityOption(Configuration.MEDIA_QUALITY_LOWEST,
            getPhotoSizeForQuality(Configuration.MEDIA_QUALITY_LOWEST)));

        final CharSequence[] array = new CharSequence[photoQualities.size()];
        photoQualities.toArray(array);

        return array;
    }

    private Size compare(int width, int height) {
        Size size = new Size(AttrHelper.vp2px(CONSTANT_600, context), AttrHelper.vp2px(CONSTANT_600, context)); // 默认宽高
        // 获取屏幕宽度和高度
        Optional<Display> display = DisplayManager.getInstance().getDefaultDisplay(context);
        Point pt = new Point();
        display.get().getSize(pt);
        double density = display.get().getAttributes().scalDensity;
        DecimalFormat df = new DecimalFormat("#.00");

        tempWidth = Double.parseDouble(df.format(width));
        tempHeight = Double.parseDouble(df.format(height));
        double wid1 = display.get().getAttributes().width;
        double hei1 = display.get().getAttributes().height;
        double wid = Double.parseDouble(df.format(wid1));
        double hei = Double.parseDouble(df.format(hei1));

        // 计算缩放比例
        double scaleX = wid / tempWidth;
        double scaleY = hei / tempHeight;
        double scale = 1.0;
        if (scaleX > scaleY && scaleX > 1) {
            scale = scaleX;
        }
        if (scaleY > scaleX && scaleY > 1) {
            scale = scaleY;
        }
        scale = Double.parseDouble(df.format(scale));
        scaleX = Double.parseDouble(df.format(scaleX));

        String odmProduct = System.getenv("ODM_PRODUCT");
        if (odmProduct.contains("ANA")) { // 说明是模拟器 因为模拟器状态默认是平躺,这里只需跟模拟器显示一样大小就OK
            size = setAnaSize(wid, hei);
        } else { // 真机
            if (mCurrentCameraId.equals(faceBackCameraId)) { // 后置摄像头
                size = setBackSize(size, wid, density, scaleX, scale);
            } else { // 前置
                size = setFrontSize(size, wid, hei, density, scale);
            }
        }
        return size;
    }

    private Size setAnaSize(double wid, double hei) {
        Size size = new Size(AttrHelper.vp2px(CONSTANT_600, context), AttrHelper.vp2px(CONSTANT_600, context)); // 默认宽高
        double tempHei = 0.0;
        if (mediaAction == Configuration.MEDIA_ACTION_VIDEO) { // 录像
            int mediaVideoQuality = configurationProvider.getMediaVideoQuality();
            if (mediaVideoQuality == Configuration.MEDIA_QUALITY_HIGH) {
                tempHei = hei * CONSTANT_0_9;
            } else {
                tempHei = hei * CONSTANT_06;
            }
        } else {
            int mediaPhotoQuality = configurationProvider.getMediaPhotoQuality(); // 拍照
            if (mediaPhotoQuality == Configuration.MEDIA_QUALITY_HIGHEST) {
                tempHei = hei * CONSTANT_0_9;
            } else if (mediaPhotoQuality == Configuration.MEDIA_QUALITY_HIGH) {
                tempHei = hei * CONSTANT_0_9;
            } else if (mediaPhotoQuality == Configuration.MEDIA_QUALITY_MEDIUM) {
                tempHei = hei * CONSTANT_0_7;
            } else {
                tempHei = hei * CONSTANT_0_6;
            }
        }
        size.setWidth((int) wid);
        size.setHeight((int) tempHei);
        return size;
    }

    private Size setBackSize(Size size, double wid, double density, double scaleX, double scale) {
        double wid1 = wid;
        double tempHeight1 = tempHeight;
        double tempWidth1 = tempWidth;
        double scale1;
        if (mediaAction == Configuration.MEDIA_ACTION_VIDEO) { // 视频
            int mediaVideoQuality = configurationProvider.getMediaVideoQuality();
            if (mediaVideoQuality == Configuration.MEDIA_QUALITY_HIGH) {
                tempHeight1 = wid1 / (scale / CONSTANT_3);
                size.setWidth((int) (wid1 * CONSTANT_1_5));
                size.setHeight((int) tempHeight1);
            } else if (mediaVideoQuality == Configuration.MEDIA_QUALITY_MEDIUM) {
                tempHeight1 = wid1 / (scale / CONSTANT_5);
                size.setWidth((int) (wid1 * CONSTANT_1_6));
                size.setHeight((int) tempHeight1);
            } else {
                tempHeight1 = wid1 / (scale / CONSTANT_8);
                size.setWidth((int) (wid1 * CONSTANT_1_6));
                size.setHeight((int) tempHeight1);
            }
        } else {
            int mediaPhotoQuality = configurationProvider.getMediaPhotoQuality(); // 拍照
            if (mediaPhotoQuality == Configuration.MEDIA_QUALITY_HIGHEST) {
                tempWidth1 = wid1 * CONSTANT_1_1;
                tempHeight1 = wid1 * CONSTANT_1_1;
                size.setWidth((int) tempWidth1);
                size.setHeight((int) tempHeight1);
            } else if (mediaPhotoQuality == Configuration.MEDIA_QUALITY_HIGH) {
                tempWidth1 = wid1 * CONSTANT_1_1;
                tempHeight1 = wid1 * CONSTANT_1_1;
                size.setWidth((int) tempWidth1);
                size.setHeight((int) tempHeight1);
            } else if (mediaPhotoQuality == Configuration.MEDIA_QUALITY_MEDIUM) {
                if (1 < scaleX) {
                    tempWidth1 = tempWidth1 / scaleX;
                } else {
                    tempWidth1 = tempWidth1 / (scaleX + CONSTANT_1_5);
                }
                size.setWidth((int) tempWidth1);
                size.setHeight((int) tempWidth1);
            } else {
                scale1 = (scale - density) / CONSTANT_1_35;
                tempHeight1 = tempHeight1 * scale1;
                size.setWidth((int) wid1);
                size.setHeight((int) tempHeight1);
            }
        }
        return size;
    }

    private Size setFrontSize(Size size, double wid, double hei, double density, double scale) {
        double tempHeight1 = tempHeight;
        double scale1;
        if (mediaAction == Configuration.MEDIA_ACTION_VIDEO) { // 视频
            int mediaVideoQuality = configurationProvider.getMediaVideoQuality();
            if (mediaVideoQuality == Configuration.MEDIA_QUALITY_HIGH) {
                tempHeight1 = wid / (scale / CONSTANT_3_5);
                size.setWidth((int) (wid * CONSTANT_1_2));
                size.setHeight((int) tempHeight1);
            } else if (mediaVideoQuality == Configuration.MEDIA_QUALITY_MEDIUM) {
                tempHeight1 = wid / (scale / CONSTANT_6);
                size.setWidth((int) (wid * CONSTANT_1_35));
                size.setHeight((int) tempHeight1);
            } else {
                tempHeight1 = wid / (scale / CONSTANT_10);
                size.setWidth((int) (wid * CONSTANT_1_5));
                size.setHeight((int) tempHeight1);
            }
        } else {
            int mediaPhotoQuality = configurationProvider.getMediaPhotoQuality(); // 拍照
            if (mediaPhotoQuality == Configuration.MEDIA_QUALITY_HIGHEST) {
                tempHeight1 = wid / (scale / CONSTANT_1_3);
                size.setWidth((int) wid);
                size.setHeight((int) tempHeight1);
            } else if (mediaPhotoQuality == Configuration.MEDIA_QUALITY_HIGH) {
                tempHeight1 = wid / (scale / CONSTANT_1_35);
                size.setWidth((int) wid);
                size.setHeight((int) tempHeight1);
            } else if (mediaPhotoQuality == Configuration.MEDIA_QUALITY_MEDIUM) {
                tempHeight1 = wid / (scale / CONSTANT_3_4);
                if (tempHeight1 > hei) {
                    tempHeight1 = tempHeight1 * CONSTANT_0_6;
                }
                size.setWidth((int) (wid * CONSTANT_1_1));
                size.setHeight((int) tempHeight1);
            } else {
                scale1 = scale - density;
                size.setWidth((int) wid);
                size.setHeight((int) (tempHeight1 * scale1));
            }
        }
        return size;
    }

    /**
     * 帧捕获状态回调
     *
     * @since 2021-05-25
     */
    private class FrameStateCallBack extends FrameStateCallback {
        @Override
        public void onFrameStarted(Camera camera, FrameConfig frameConfig, long frameNumber, long timestamp) {
        }

        @Override
        public void onFrameProgressed(Camera camera, FrameConfig frameConfig, FrameResult frameResult) {
        }

        @Override
        public void onFrameFinished(Camera camera, FrameConfig frameConfig, FrameResult frameResult) {
        }

        @Override
        public void onFrameError(Camera camera, FrameConfig frameConfig, int errorCode, FrameResult frameResult) {
        }

        @Override
        public void onCaptureTriggerStarted(Camera camera, int captureTriggerId, long firstFrameNumber) {
            updatePreview();
        }

        @Override
        public void onCaptureTriggerFinished(Camera camera, int captureTriggerId, long lastFrameNumber) {
        }

        @Override
        public void onCaptureTriggerInterrupted(Camera camera, int captureTriggerId) {
        }
    }

    /**
     * 相机状态回调
     *
     * @since 2021-05-25
     */
    private class CameraStateCallbackImpl extends CameraStateCallback {
        @Override
        public void onCreated(Camera camera) {
            // 创建相机设备
            cameraCallback = camera;

            CameraConfig.Builder cameraConfigBuilder = camera.getCameraConfigBuilder();
            if (cameraConfigBuilder == null) {
                Log.error(TAG, "onCreated cameraConfigBuilder is null");
                return;
            }
            cameraConfigBuilder.addSurface(surface); // 配置预览的Surface
            cameraConfigBuilder.addSurface(recevingSurface); // 配置拍照的Surface
            cameraConfigBuilder.addSurface(videoRecorder.getVideoSurface()); // 配置录像的Surface
            FrameStateCallBack frameStateCallbackImpl = new FrameStateCallBack(); // 配置帧结果的回调
            cameraConfigBuilder.setFrameStateCallback(frameStateCallbackImpl, uiHandler);
            try {
                // 相机设备配置
                camera.configure(cameraConfigBuilder.build());
            } catch (IllegalArgumentException error) {
                Log.error(TAG, "Argument Exception " + error.getLocalizedMessage());
            } catch (IllegalStateException err) {
                Log.error(TAG, "State Exception " + err.getLocalizedMessage());
            }
        }

        @Override
        public void onConfigured(Camera camera) {
            // 配置相机设备
            // 获取预览配置模板
            frameConfigBuilder = camera.getFrameConfigBuilder(FRAME_CONFIG_PREVIEW);
            frameConfigBuilder.addSurface(surface); // 配置预览Surface
            previewFrameConfig = frameConfigBuilder.build();
            try {
                // 启动循环帧捕获
                // 调用triggerLoopingCapture方法实现预览帧配置更新
                camera.triggerLoopingCapture(previewFrameConfig);
            } catch (IllegalArgumentException error) {
                Log.error(TAG, "Argument Exception " + error.getLocalizedMessage());
            } catch (IllegalStateException err) {
                Log.error(TAG, "State Exception " + err.getLocalizedMessage());
            }
        }

        @Override
        public void onPartialConfigured(Camera camera) {
            // 当使用了addDeferredSurfaceSize配置了相机，会接到此回调
        }

        @Override
        public void onReleased(Camera camera) {
            // 释放相机设备
        }
    }

    /**
     * Surface创建回调
     *
     * @since 2021-05-25
     */
    private class SurfaceCallback implements SurfaceOps.Callback {
        @Override
        public void surfaceCreated(SurfaceOps surfaceOps) {
            surface = surfaceOps.getSurface(); // 创建相机
            cameraStateCallback = new CameraStateCallbackImpl();
            cameraKit.createCamera(cameraIdString, cameraStateCallback, uiHandler);
            setSurfaceSize();
        }

        @Override
        public void surfaceChanged(SurfaceOps surfaceOps, int ii, int i1, int i2) {
        }

        @Override
        public void surfaceDestroyed(SurfaceOps surfaceOps) {
        }
    }

    /**
     * 打开相机预览页面回调
     *
     * @since 2021-04-10
     */
    private class OpenCameraSurfaceCallback implements SurfaceOps.Callback {
        @Override
        public void surfaceCreated(SurfaceOps surfaceOps) {
            if (surfaceOps.getSurface() == null) {
                return;
            }
            surface = surfaceOps.getSurface();
            if (cameraCallback != null) {
                cameraCallback.stopLoopingCapture(); // 停止循环帧捕获(停止预览)
            }
            startPreview();
        }

        @Override
        public void surfaceChanged(SurfaceOps surfaceOps, int ii, int i1, int i2) {
            if (surfaceOps.getSurface() == null) {
                return;
            }
            surface = surfaceOps.getSurface();
            cameraCallback.stopLoopingCapture(); // 停止循环帧捕获(停止预览)
            startPreview();
        }

        @Override
        public void surfaceDestroyed(SurfaceOps surfaceOps) {
        }
    }
}
