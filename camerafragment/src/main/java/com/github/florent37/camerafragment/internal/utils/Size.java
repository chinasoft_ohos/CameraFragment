/*
 * Copyright (C) 2021 Huawei Device Co., Ltd.
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 */

package com.github.florent37.camerafragment.internal.utils;

import java.util.ArrayList;
import java.util.List;

/**
 * size
 *
 * @since 2021-04-09
 */
public class Size {
    private int width;
    private int height;

    /**
     * 构造参数
     */
    public Size() {
        width = 0;
        height = 0;
    }

    /**
     * 带参数构造
     *
     * @param width 宽度
     * @param height 高度
     */
    public Size(int width, int height) {
        this.width = width;
        this.height = height;
    }

    /**
     * 获取大小
     *
     * @param size 传入系统Size
     */
    public Size(ohos.media.image.common.Size size) {
        this.width = size.width;
        this.height = size.height;
    }

    public int getWidth() {
        return width;
    }

    public void setWidth(int width) {
        this.width = width;
    }

    public int getHeight() {
        return height;
    }

    public void setHeight(int height) {
        this.height = height;
    }

    /**
     * 把系统Size 转成自定义Size
     *
     * @param sizes 传入系统集合size
     * @return 集合size
     */
    public static List<Size> fromList2(List<ohos.media.image.common.Size> sizes) {
        if (sizes == null) {
            return new ArrayList<>();
        }
        List<Size> result = new ArrayList<>(sizes.size());

        for (ohos.media.image.common.Size size : sizes) {
            result.add(new Size(size));
        }

        return result;
    }

    /**
     * 把系统Size 转成自定义Size
     *
     * @param sizes 传入系统数组size
     * @return 数组size
     */
    public static Size[] fromArray2(ohos.media.image.common.Size[] sizes) {
        if (sizes == null) {
            return new Size[]{};
        }
        Size[] result = new Size[sizes.length];

        for (int index = 0; index < sizes.length; ++index) {
            result[index] = new Size(sizes[index]);
        }

        return result;
    }
}
