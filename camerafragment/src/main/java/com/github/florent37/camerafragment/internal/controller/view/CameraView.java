/*
 * Copyright (C) 2021 Huawei Device Co., Ltd.
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 */

package com.github.florent37.camerafragment.internal.controller.view;

import com.github.florent37.camerafragment.configuration.Configuration;
import com.github.florent37.camerafragment.internal.utils.Size;
import com.github.florent37.camerafragment.listeners.CameraFragmentResultListener;

import ohos.agp.components.Component;

/**
 * 相机视图
 *
 * @since 2021-05-24
 */
public interface CameraView {
    /**
     * 更新相机预览页面
     *
     * @param size 预览大小
     * @param cameraPreview 预览视图
     */
    void updateCameraPreview(Size size, Component cameraPreview);

    /**
     * 在UI线程更新媒体操作
     *
     * @param mediaAction 媒体操作
     */
    void updateUiForMediaAction(@Configuration.MediaAction int mediaAction);

    /**
     * 更新相机切换器
     *
     * @param numberOfCameras 切换器数量
     */
    void updateCameraSwitcher(int numberOfCameras);

    /**
     * 拍照
     *
     * @param callback 拍照回调
     */
    void onPhotoTaken(CameraFragmentResultListener callback);

    /**
     * 开始录像
     *
     * @param width 宽度
     * @param height 高度
     */
    void onVideoRecordStart(int width, int height);

    /**
     * 停止录像
     *
     * @param callback 接口回调
     */
    void onVideoRecordStop(CameraFragmentResultListener callback);

    /**
     * 释放相机预览视图
     */
    void releaseCameraPreview();
}
