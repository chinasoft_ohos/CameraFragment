/*
 * Copyright (C) 2021 Huawei Device Co., Ltd.
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 */

package com.github.florent37.camerafragment.internal.manager;

import com.github.florent37.camerafragment.configuration.Configuration;
import com.github.florent37.camerafragment.configuration.ConfigurationProvider;
import com.github.florent37.camerafragment.internal.manager.listener.CameraCloseListener;
import com.github.florent37.camerafragment.internal.manager.listener.CameraOpenListener;
import com.github.florent37.camerafragment.internal.manager.listener.CameraPhotoListener;
import com.github.florent37.camerafragment.internal.manager.listener.CameraVideoListener;
import com.github.florent37.camerafragment.internal.utils.Size;
import com.github.florent37.camerafragment.listeners.CameraFragmentResultListener;

import ohos.aafwk.ability.fraction.FractionAbility;
import ohos.agp.components.DirectionalLayout;
import ohos.app.Context;

import java.io.File;

/**
 * 相机管理接口
 *
 * @param <Integer>
 * @param <SurfaceListener>
 * @since 2021-05-24
 */
public interface CameraManager<Integer, SurfaceListener> {
    /**
     * 初始化相机管理
     *
     * @param configurationProvider 配置项接口
     * @param context 上下文
     */
    void initializeCameraManager(ConfigurationProvider configurationProvider, Context context);

    /**
     * 打开相机
     *
     * @param cameraContext 上下文
     * @param currentCameraId 当前相机Id
     * @param cameraOpenListener 打开相机回调
     */
    void openCamera(Context cameraContext, int currentCameraId,
                    CameraOpenListener<Integer, SurfaceListener> cameraOpenListener);

    /**
     * 关闭相机
     *
     * @param cameraCloseListener 关闭相机回调
     */
    void closeCamera(CameraCloseListener<Integer> cameraCloseListener);

    /**
     * 设置闪光灯模式
     *
     * @param flashMode 闪光灯类型
     */
    void setFlashMode(@Configuration.FlashMode int flashMode);

    /**
     * 拍照
     *
     * @param photoFile 拍照文件
     * @param cameraPhotoListener 拍照监听
     * @param callback Fraction处理结果回调
     */
    void takePhoto(File photoFile, CameraPhotoListener cameraPhotoListener, CameraFragmentResultListener callback);

    /**
     * 开始录像
     *
     * @param videoFile 录像文件
     * @param cameraVideoListener 录像监听
     */
    void startVideoRecord(File videoFile, CameraVideoListener cameraVideoListener);

    /**
     * 获取图片质量大小
     *
     * @param mediaQuality 质量类型
     * @return Size
     */
    Size getPhotoSizeForQuality(@Configuration.MediaQuality int mediaQuality);

    /**
     * 停止录像
     *
     * @param callback Fraction处理结果回调
     */
    void stopVideoRecord(CameraFragmentResultListener callback);

    /**
     * 释放相机管理
     */
    void releaseCameraManager();

    /**
     * 获取当前相机Id
     *
     * @return int
     */
    int getCurrentCameraId();

    /**
     * 获取前置摄像头Id
     *
     * @return int
     */
    int getFaceFrontCameraId();

    /**
     * 获取后置摄像头Id
     *
     * @return int
     */
    int getFaceBackCameraId();

    /**
     * 获取相机数量
     *
     * @return int
     */
    int getNumberOfCameras();

    /**
     * 获取前置相机方向
     *
     * @return int
     */
    int getFaceFrontCameraOrientation();

    /**
     * 获取后置相机方向
     *
     * @return int
     */
    int getFaceBackCameraOrientation();

    /**
     * 是否正在录制
     *
     * @return boolean
     */
    boolean isVideoRecording();

    /**
     * 获取录像画面分辨率选项
     *
     * @return CharSequence[]
     */
    CharSequence[] getVideoQualityOptions();

    /**
     * 获取拍照画面分辨率选项
     *
     * @return CharSequence[]
     */
    CharSequence[] getPhotoQualityOptions();

    /**
     * 设置当前相机Id
     *
     * @param currentCameraId 当前相机Id
     */
    void setCameraId(java.lang.Integer currentCameraId);

    /**
     * 初始化预览视图
     *
     * @param layout 线性布局
     * @param ability 上下文
     * @param listener 打开相机回调
     */
    void initSurfaceComponent(DirectionalLayout layout, FractionAbility ability,
                                  CameraOpenListener<Integer, SurfaceListener> listener);

    /**
     * 设置媒体操作
     *
     * @param mediaAction 操作类型
     */
    void setMediaAction(int mediaAction);

    /**
     * 准备录像录制
     */
    void prepareVideoRecorder();

    /**
     * 设置屏幕旋转
     *
     * @param degrees 旋转角度
     */
    void setScreenRotation(int degrees);

    /**
     * 释放相机
     */
    void releaseCameraResource();
}
