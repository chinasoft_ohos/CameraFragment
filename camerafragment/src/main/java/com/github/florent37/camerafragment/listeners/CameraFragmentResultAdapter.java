/*
 * Copyright (C) 2021 Huawei Device Co., Ltd.
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 */

package com.github.florent37.camerafragment.listeners;

/**
 * 相机Fraction处理结果Adapter
 *
 * @since 2021-05-25
 */
public class CameraFragmentResultAdapter implements CameraFragmentResultListener {
    @Override
    public void onVideoRecorded(String filePath) {
    }

    @Override
    public void onPhotoTaken(String filePath) {
    }
}
