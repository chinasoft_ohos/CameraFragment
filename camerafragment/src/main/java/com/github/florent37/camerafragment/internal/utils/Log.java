/*
 * Copyright (C) 2021 Huawei Device Co., Ltd.
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 */

package com.github.florent37.camerafragment.internal.utils;

import ohos.hiviewdfx.HiLog;
import ohos.hiviewdfx.HiLogLabel;

/**
 * Log打印
 *
 * @since 2021-05-25
 */
public class Log {
    private static final HiLogLabel LABEL = new HiLogLabel(HiLog.LOG_APP, 0x01, "CameraFragment");
    private static final String BLANK = " ";

    private Log() {
    }

    /**
     * 替换格式
     *
     * @param logMessageFormat 信息格式
     * @return String
     */
    public static String replaceFormat(String logMessageFormat) {
        return logMessageFormat.replaceAll("%([d|f|s])", "%{public}$1");
    }

    /**
     * debug
     *
     * @param tag 标签
     * @param format 格式
     */
    public static void debug(String tag, String format) {
        HiLog.debug(LABEL, tag + BLANK + replaceFormat(format));
    }

    /**
     * info
     *
     * @param tag 标签
     * @param format 格式
     */
    public static void info(String tag, String format) {
        HiLog.info(LABEL, tag + BLANK + replaceFormat(format));
    }

    /**
     * warn
     *
     * @param tag 标签
     * @param format 格式
     */
    public static void warn(String tag, String format) {
        HiLog.warn(LABEL, tag + BLANK + replaceFormat(format));
    }

    /**
     * error
     *
     * @param tag 标签
     * @param format 格式
     */
    public static void error(String tag, String format) {
        HiLog.error(LABEL, tag + BLANK + replaceFormat(format));
    }
}
