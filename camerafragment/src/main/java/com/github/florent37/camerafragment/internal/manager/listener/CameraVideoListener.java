/*
 * Copyright (C) 2021 Huawei Device Co., Ltd.
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 */

package com.github.florent37.camerafragment.internal.manager.listener;

import com.github.florent37.camerafragment.internal.utils.Size;
import com.github.florent37.camerafragment.listeners.CameraFragmentResultListener;

import java.io.File;

/**
 * 录像接口
 *
 * @since 2021-05-24
 */
public interface CameraVideoListener {
    /**
     * 开始录像
     *
     * @param videoSize 录像大小
     */
    void onVideoRecordStarted(Size videoSize);

    /**
     * 停止录像
     *
     * @param videoFile 录像文件
     * @param callback 录像结果处理
     */
    void onVideoRecordStopped(File videoFile, CameraFragmentResultListener callback);
}
