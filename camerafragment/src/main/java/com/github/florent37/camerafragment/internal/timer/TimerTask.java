/*
 * Copyright (C) 2021 Huawei Device Co., Ltd.
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 */

package com.github.florent37.camerafragment.internal.timer;

import com.github.florent37.camerafragment.internal.utils.DateTimeUtils;

import java.util.Locale;

/**
 * 时间任务
 *
 * @since 2021-05-25
 */
public class TimerTask extends TimerTaskBase implements Runnable {
    private static final int CONSTANT_60 = 60;

    /**
     * 构造函数
     *
     * @param callback 任务回调
     */
    public TimerTask(TimerTaskBase.Callback callback) {
        super(callback);
    }

    @Override
    public void run() {
        recordingTimeSeconds++;

        if (recordingTimeSeconds == CONSTANT_60) {
            recordingTimeSeconds = 0;
            recordingTimeMinutes++;
        }
        if (callback != null) {
            callback.setText(
                String.format(
                    Locale.getDefault(),
                    "%02d:%02d",
                    recordingTimeMinutes,
                    recordingTimeSeconds
                )
            );
        }
        if (isAlive) {
            handler.postTask(this, DateTimeUtils.SECOND);
        }
    }

    /**
     * 开始计时
     */
    public void start() {
        isAlive = true;
        recordingTimeMinutes = 0;
        recordingTimeSeconds = 0;
        if (callback != null) {
            callback.setText(
                String.format(
                    Locale.getDefault(),
                    "%02d:%02d",
                    recordingTimeMinutes,
                    recordingTimeSeconds
                )
            );
            callback.setTextVisible(true);
        }
        handler.postTask(this, DateTimeUtils.SECOND);
    }

    /**
     * 停止计时
     */
    public void stop() {
        if (callback != null) {
            callback.setTextVisible(false);
        }
        isAlive = false;
    }
}
