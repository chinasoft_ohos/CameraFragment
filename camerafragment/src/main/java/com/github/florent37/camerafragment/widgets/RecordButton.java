/*
 * Copyright (C) 2021 Huawei Device Co., Ltd.
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 */

package com.github.florent37.camerafragment.widgets;

import ohos.agp.components.AttrSet;
import ohos.agp.components.Button;
import ohos.agp.components.Component;
import ohos.app.Context;

/**
 * 录制按钮
 *
 * @since 2021-05-25
 */
public class RecordButton extends Button {
    /**
     * 录制按钮点击监听
     *
     * @since 2021-05-25
     */
    public interface RecordButtonListener {
        /**
         * 录制按钮点击
         */
        void onRecordButtonClicked();
    }

    private RecordButtonListener listener;

    /**
     * 构造函数
     *
     * @param context 上下文
     */
    public RecordButton(Context context) {
        this(context, null);
    }

    /**
     * 构造函数
     *
     * @param context 上下文
     * @param attrSet attr集合
     */
    public RecordButton(Context context, AttrSet attrSet) {
        this(context, attrSet, null);
    }

    /**
     * 构造函数
     *
     * @param context 上下文
     * @param attrSet attr集合
     * @param styleName 类型名字
     */
    public RecordButton(Context context, AttrSet attrSet, String styleName) {
        super(context, attrSet, styleName);
        setClickedListener(new ClickedListener() {
            int clickDelay = 1000;
            long lastClickTime = 0L;

            @Override
            public void onClick(Component component) {
                if (System.currentTimeMillis() - lastClickTime < clickDelay) {
                    return;
                } else {
                    lastClickTime = System.currentTimeMillis();
                }

                if (listener != null) {
                    listener.onRecordButtonClicked();
                }
            }
        });
    }

    /**
     * 录制按钮点击监听方法
     *
     * @param recordButtonListener 监听
     */
    public void setRecordButtonListener(RecordButtonListener recordButtonListener) {
        this.listener = recordButtonListener;
    }
}
