/*
 * Copyright (C) 2021 Huawei Device Co., Ltd.
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 */

package com.github.florent37.camerafragment.internal.enums;

import com.github.florent37.camerafragment.configuration.Configuration;

import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;

/**
 * 录制
 *
 * @since 2021-05-24
 */
public class Record {
    /**
     * 拍照状态
     */
    public static final int TAKE_PHOTO_STATE = 0;
    /**
     * 录像状态
     */
    public static final int READY_FOR_RECORD_STATE = 1;
    /**
     * 记录在进行状态
     */
    public static final int RECORD_IN_PROGRESS_STATE = 2;

    /**
     * 构造函数
     */
    private Record() {
    }

    /**
     * 录像注解
     *
     * @since 2021-05-24
     */
    @Configuration.IntDef({TAKE_PHOTO_STATE, READY_FOR_RECORD_STATE, RECORD_IN_PROGRESS_STATE})
    @Retention(RetentionPolicy.SOURCE)
    public @interface RecordState {
    }
}
