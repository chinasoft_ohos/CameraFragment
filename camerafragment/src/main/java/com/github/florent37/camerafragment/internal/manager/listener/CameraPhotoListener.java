/*
 * Copyright (C) 2021 Huawei Device Co., Ltd.
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 */

package com.github.florent37.camerafragment.internal.manager.listener;

import com.github.florent37.camerafragment.listeners.CameraFragmentResultListener;

import java.io.File;

/**
 * 相机拍照接口
 *
 * @since 2021-05-24
 */
public interface CameraPhotoListener {
    /**
     * 拍照
     *
     * @param photoFile 文件
     * @param callback 拍照结果处理回调
     */
    void onPhotoTaken(File photoFile, CameraFragmentResultListener callback);

    /**
     * 拍照失败
     */
    void onPhotoTakeError();
}
