/*
 * Copyright (C) 2021 Huawei Device Co., Ltd.
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 */

package com.github.florent37.camerafragment.internal.enums;

import com.github.florent37.camerafragment.configuration.Configuration;

import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;

/**
 * 闪光灯常量
 *
 * @since 2021-05-24
 */
public class Flash {
    /**
     * 闪光灯开
     */
    public static final int FLASH_ON = 0;
    /**
     * 闪光灯关
     */
    public static final int FLASH_OFF = 1;
    /**
     * 闪光灯自动
     */
    public static final int FLASH_AUTO = 2;

    /**
     * 构造函数
     */
    private Flash() {
    }

    /**
     * 闪光灯注解
     *
     * @since 2021-05-24
     */
    @Configuration.IntDef({FLASH_ON, FLASH_OFF, FLASH_AUTO})
    @Retention(RetentionPolicy.SOURCE)
    public @interface FlashMode {
    }
}
