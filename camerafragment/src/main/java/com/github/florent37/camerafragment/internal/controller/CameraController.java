/*
 * Copyright (C) 2021 Huawei Device Co., Ltd.
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 */

package com.github.florent37.camerafragment.internal.controller;

import com.github.florent37.camerafragment.configuration.Configuration;
import com.github.florent37.camerafragment.internal.manager.CameraManager;
import com.github.florent37.camerafragment.listeners.CameraFragmentResultListener;

import ohos.aafwk.ability.fraction.FractionAbility;
import ohos.aafwk.content.Intent;
import ohos.agp.components.DirectionalLayout;

import java.io.File;

/**
 * 相机操作
 *
 * @param <Integer>
 * @since 2021-05-24
 */
public interface CameraController<Integer> {
    /**
     * 初始化创建相机
     *
     * @param savedInstanceState intent状态
     * @param surfaceProvider 线性布局
     * @param fractionAbility Ability页面
     */
    void onCreate(Intent savedInstanceState, DirectionalLayout surfaceProvider, FractionAbility fractionAbility);

    /**
     * 失去焦点时释放相机,重新打开相机
     */
    void onPause();

    /**
     * 页面销毁时
     */
    void onDestroy();

    /**
     * 拍照1
     *
     * @param resultListener 拍照结果回调
     */
    void takePhoto(CameraFragmentResultListener resultListener);

    /**
     * 拍照2
     *
     * @param callback 拍照结果回调
     * @param direcoryPath 路径
     * @param fileName 文件名字
     */
    void takePhoto(CameraFragmentResultListener callback, String direcoryPath, String fileName);

    /**
     * 开始录像
     */
    void startVideoRecord();

    /**
     * 开始录像
     *
     * @param direcoryPath 文件路径
     * @param fileName 文件名字
     */
    void startVideoRecord(String direcoryPath, String fileName);

    /**
     * 停止录像
     *
     * @param callback 结果回调
     */
    void stopVideoRecord(CameraFragmentResultListener callback);

    /**
     * 是否正在录像
     *
     * @return boolean处理结果
     */
    boolean isVideoRecording();

    /**
     * 选择相机
     *
     * @param cameraFace 相机前/后置
     */
    void switchCamera(@Configuration.CameraFace int cameraFace);

    /**
     * 选择画面质量
     */
    void switchQuality();

    /**
     * 设置闪光灯模式
     *
     * @param flashMode 模式
     */
    void setFlashMode(@Configuration.FlashMode int flashMode);

    /**
     * 获取相机设备数量
     *
     * @return int整数
     */
    int getNumberOfCameras();

    /**
     * 获取媒体操作
     *
     * @return int整数
     */
    @Configuration.MediaAction
    int getMediaAction();

    /**
     * 设置媒体操作
     *
     * @param mediaAction 媒体操作
     */
    void setMediaAction(int mediaAction);

    /**
     * 获取当前相机ID
     *
     * @return Integer整数包装类
     */
    int getCurrentCameraId();

    /**
     * 输出文件
     *
     * @return File文件
     */
    File getOutputFile();

    /**
     * 获取相机管理
     *
     * @return CameraManager相机管理者
     */
    CameraManager getCameraManager();

    /**
     * 获取录像画面选项
     *
     * @return CharSequence[] 像素选项
     */
    CharSequence[] getVideoQualityOptions();

    /**
     * 获取拍照画面选项
     *
     * @return CharSequence[]像素选项
     */
    CharSequence[] getPhotoQualityOptions();

    /**
     * 打开闪光灯
     */
    void openFlashLight();

    /**
     * 关闭闪光灯
     */
    void closeFlashLight();

    /**
     * 设置屏幕旋转
     *
     * @param degrees 旋转角度
     */
    void setScreenRotation(int degrees);

    /**
     * 释放相机
     */
    void releaseCamera();

    /**
     * 打开相机
     */
    void openCamera();
}
