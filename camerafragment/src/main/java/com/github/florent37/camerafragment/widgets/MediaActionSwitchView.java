/*
 * Copyright (C) 2021 Huawei Device Co., Ltd.
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 */

package com.github.florent37.camerafragment.widgets;

import com.github.florent37.camerafragment.ResourceTable;
import com.github.florent37.camerafragment.internal.utils.Utils;

import ohos.agp.components.AttrHelper;
import ohos.agp.components.AttrSet;
import ohos.agp.components.Component;
import ohos.agp.components.Image;
import ohos.agp.components.element.ShapeElement;
import ohos.app.Context;
import ohos.media.image.PixelMap;
import ohos.multimodalinput.event.TouchEvent;

/**
 * 相机/录像切换
 *
 * @since 2021-05-25
 */
public class MediaActionSwitchView extends Image implements Component.TouchEventListener {
    private static final int CONSTANT_2 = 2;
    private static final int CONSTANT_5 = 5;
    private static final float CONSTANT_0_5 = 0.5f;
    private int padding;
    private PixelMap photoPixelMap1;
    private PixelMap photoPixelMap2;
    private PixelMap videoPixelMap1;
    private PixelMap videoPixelMap2;
    private boolean isClick = false;
    private boolean isMove = false;
    private float downY;
    private float downX;
    private OnMediaActionStateChangeListener onMediaActionStateChangeListener;

    /**
     * 构造函数
     *
     * @param context 上下文
     */
    public MediaActionSwitchView(Context context) {
        this(context, null);
    }

    /**
     * 构造函数
     *
     * @param context 上下文
     * @param attrSet attr集合
     */
    public MediaActionSwitchView(Context context, AttrSet attrSet) {
        this(context, attrSet, null);
    }

    /**
     * 构造函数
     *
     * @param context 上下文
     * @param attrSet attr集合
     * @param styleName 类型名字
     */
    public MediaActionSwitchView(Context context, AttrSet attrSet, String styleName) {
        super(context, attrSet, styleName);
        initializeView();
    }

    private void initializeView() {
        Context context = getContext();
        photoPixelMap1 = Utils.getPixelMap(context, ResourceTable.Media_ic_photo_camera_white_24dp);
        photoPixelMap2 = Utils.getPixelMap(getContext(), ResourceTable.Media_ic_photo_camera_yellow_24dp);
        videoPixelMap1 = Utils.getPixelMap(context, ResourceTable.Media_ic_videocam_white_24dp);
        videoPixelMap2 = Utils.getPixelMap(getContext(), ResourceTable.Media_ic_videocam_yellow_24dp);

        ShapeElement shapeElement = new ShapeElement(getContext(),
            ResourceTable.Graphic_circle_frame_background_dark);
        setBackground(shapeElement); // 设置背景颜色
        setClickedListener(new ClickedListener() {
            @Override
            public void onClick(Component component) {
                if (onMediaActionStateChangeListener != null) {
                    onMediaActionStateChangeListener.switchAction();
                }
            }
        });

        padding = AttrHelper.vp2px(CONSTANT_5, context);
        setPadding(padding, padding, padding, padding);
        setTouchEventListener(this);
        displayActionWillSwitchVideo();
    }

    /**
     * 显示拍照按钮
     */
    public void displayActionWillSwitchPhoto() {
        setPixelMap(photoPixelMap1);
    }

    /**
     * 显示录像按钮
     */
    public void displayActionWillSwitchVideo() {
        setPixelMap(videoPixelMap1);
    }

    public void setOnMediaActionStateChangeListener(OnMediaActionStateChangeListener onMediaActionStateChangeListener) {
        this.onMediaActionStateChangeListener = onMediaActionStateChangeListener;
    }

    /**
     * 拍照/录像按钮切换监听
     *
     * @since 2021-05-25
     */
    public interface OnMediaActionStateChangeListener {
        /**
         * 切换动作
         */
        void switchAction();
    }

    @Override
    public void setEnabled(boolean isEnabled) {
        super.setEnabled(isEnabled);
        if (isEnabled) {
            setAlpha(1f);
        } else {
            setAlpha(CONSTANT_0_5);
        }
    }

    @Override
    public boolean onTouchEvent(Component component, TouchEvent touchEvent) {
        switch (touchEvent.getAction()) {
            case TouchEvent.PRIMARY_POINT_DOWN: // 手指点下
                downX = touchEvent.getPointerScreenPosition(0).getX();
                downY = touchEvent.getPointerScreenPosition(0).getY();
                if (!isClick) {
                    setPixelMap(videoPixelMap2);
                } else {
                    setPixelMap(photoPixelMap2);
                }
                isMove = false;
                break;
            case TouchEvent.POINT_MOVE: // 手指在移动
                float moveX = touchEvent.getPointerScreenPosition(0).getX();
                float moveY = touchEvent.getPointerScreenPosition(0).getY();
                float width = getWidth();
                float height = getHeight();
                if (Math.abs(moveX - downX) > width / CONSTANT_2
                    || Math.abs(moveY - downY) > height / CONSTANT_2) {
                    // 取消当前图片颜色,恢复白色图片
                    setPixelMap(!isClick ? videoPixelMap1 : photoPixelMap1);
                    isMove = true;
                }
                break;
            case TouchEvent.PRIMARY_POINT_UP: // 手指抬起来
                if (!isMove) {
                    if (!isClick) {
                        setPixelMap(photoPixelMap1);
                        isClick = true;
                    } else {
                        setPixelMap(videoPixelMap1);
                        isClick = false;
                    }
                }
                break;
            default:
                break;
        }
        return true;
    }
}
