/*
 * Copyright (C) 2021 Huawei Device Co., Ltd.
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 */

package com.github.florent37.camerafragment.widgets;

import com.github.florent37.camerafragment.ResourceTable;
import com.github.florent37.camerafragment.internal.utils.Utils;

import ohos.agp.components.AttrHelper;
import ohos.agp.components.AttrSet;
import ohos.agp.components.Image;
import ohos.app.Context;
import ohos.media.image.PixelMap;

/**
 * 闪光灯切换
 *
 * @since 2021-05-25
 */
public class FlashSwitchView extends Image {
    private static final int PADDING = 5;
    private final int padding;
    private PixelMap flashOnPixelMap;
    private PixelMap flashOffPixelMap;
    private PixelMap flashAutoPixelMap;

    /**
     * 构造函数
     *
     * @param context 上下文
     */
    public FlashSwitchView(Context context) {
        this(context, null);
    }

    /**
     * 构造函数
     *
     * @param context 上下文
     * @param attrSet attr集合
     */
    public FlashSwitchView(Context context, AttrSet attrSet) {
        this(context, attrSet, null);
    }

    /**
     * 构造函数
     *
     * @param context 上下文
     * @param attrSet attr集合
     * @param styleName 类型名字
     */
    public FlashSwitchView(Context context, AttrSet attrSet, String styleName) {
        super(context, attrSet, styleName);
        flashOnPixelMap = Utils.getPixelMap(context, ResourceTable.Media_ic_flash_on_white_24dp);
        flashOffPixelMap = Utils.getPixelMap(context, ResourceTable.Media_ic_flash_off_white_24dp);
        flashAutoPixelMap = Utils.getPixelMap(context, ResourceTable.Media_ic_flash_auto_white_24dp);
        padding = AttrHelper.vp2px(PADDING, context);
        setPadding(padding, padding, padding, padding);
        init();
    }

    private void init() {
        displayFlashAuto();
    }

    /**
     * 关闭闪光灯
     */
    public void displayFlashOff() {
        setPixelMap(flashOffPixelMap);
    }

    /**
     * 打开闪光灯
     */
    public void displayFlashOn() {
        setPixelMap(flashOnPixelMap);
    }

    /**
     * 自动闪光灯
     */
    public void displayFlashAuto() {
        setPixelMap(flashAutoPixelMap);
    }
}
