/*
 * Copyright (C) 2021 Huawei Device Co., Ltd.
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 */

package com.github.florent37.camerafragment.widgets;

import com.github.florent37.camerafragment.ResourceTable;

import ohos.agp.components.AttrHelper;
import ohos.agp.components.AttrSet;
import ohos.agp.components.Image;
import ohos.agp.components.element.ShapeElement;
import ohos.app.Context;

/**
 * 相机设置按钮
 *
 * @since 2021-05-25
 */
public class CameraSettingsView extends Image {
    private static final int PADDING = 5;
    private int padding;

    /**
     * 构造函数
     *
     * @param context 上下文
     */
    public CameraSettingsView(Context context) {
        this(context, null);
    }

    /**
     * 构造函数
     *
     * @param context 上下文
     * @param attrSet attr集合
     */
    public CameraSettingsView(Context context, AttrSet attrSet) {
        this(context, attrSet, null);
    }

    /**
     * 构造函数
     *
     * @param context 上下文
     * @param attrSet attr集合
     * @param styleName 类型名字
     */
    public CameraSettingsView(Context context, AttrSet attrSet, String styleName) {
        super(context, attrSet, styleName);
        init();
    }

    private void init() {
        Context context = getContext();
        ShapeElement shapeElement = new ShapeElement(getContext(), ResourceTable.Graphic_circle_frame_background_dark);
        setBackground(shapeElement); // 设置背景颜色
        padding = AttrHelper.vp2px(PADDING, context);
        setPadding(padding, padding, padding, padding);
        setPixelMap(ResourceTable.Media_ic_settings_white_24dp); // 设置图片
    }
}
