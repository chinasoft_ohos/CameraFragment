# CameraFragment

#### 项目介绍
- 项目名称：CameraFragment
- 所属系列：openharmony 第三方组件适配移植
- 功能：一个简单的易于集成的相机Fraction
- 项目移植状态：主功能完成
- 调用差异：无
- 开发版本：sdk6，DevEco Studio2.2 Beta1
- 基线版本：Release 1.0.10

#### 效果演示
![camerafragment_harmony](media/sample1.png)  
![camerafragment_harmony](media/sample2.png)  
![camerafragment_harmony](media/sample_small.png)  
![camerafragment_harmony](media/settings.gif)  
![camerafragment_harmony](media/switch_action.gif)  
![camerafragment_harmony](media/switch_camera.gif)  


#### 安装教程
1.在项目根目录下的build.gradle文件中,
```
allprojects {
 repositories {
     maven {
         url 'https://s01.oss.sonatype.org/content/repositories/snapshots/'
     }
 }
}
```
2.在entry模块的build.gradle文件中
```
dependencies {
    implementation 'com.gitee.chinasoft_ohos:CameraFragment:1.0.1'
}
 ```


在sdk6，DevEco Studio2.2 Beta1下项目可直接运行
如无法运行，删除项目.gradle,.idea,build,gradle,build.gradle文件，
并依据自己的版本创建新项目，将新项目的对应文件复制到根目录下


#### 使用说明
初始化后置摄像头配置
```java
CameraFraction cameraFraction = CameraFraction.newInstance(new Configuration.Builder()
.setCamera(Configuration.CAMERA_FACE_REAR).build())

// 初始化得到的 cameraFraction 添加到StackLayout中
getFractionManager().startFractionScheduler()
                .add(ResourceTable.Id_content, cameraFraction, FRAGMENT_TAG)
                .submit();
```
// 行为  
你可以直接用它拍照/录像
```java
cameraFraction.takePhotoOrCaptureVideo(callback);
cameraFraction.takePhotoOrCaptureVideo(callback, directoryPath, fileName);
```

Flash可以启用/禁用(' AUTO ' / ' OFF ' / ' ON ')

```java
cameraFraction.toggleFlashMode();


相机类型可以修改('后' / '前')

```java
cameraFraction.switchCameraTypeFrontBack();
```

你可以改变捕获的照片/视频大小

```java
cameraFraction.openSettingDialog();
```

//结果  
返回相机记录/照片的结果 `CameraFragmentResultListener`

```java
cameraFraction.setResultListener(new CameraFragmentResultListener() {
       @Override
       public void onVideoRecorded(byte[] bytes, String filePath) {
                // 视频录制完成并保存后调用
                startActivityForResult(PreviewActivity.newIntentVideo(MainActivity.this, filePath));
       }

       @Override
       public void onPhotoTaken(byte[] bytes, String filePath) {
                // 在拍摄和保存照片时调用

                startActivity(PreviewActivity.newIntentPhoto(MainActivity.this, filePath));
       }
});
```

//相机监听器  
```java
cameraFraction.setStateListener(new CameraFragmentStateListener() {

    //当前显示的相机是背面
    void onCurrentCameraBack();
    //当前显示的摄像头为正面时
    void onCurrentCameraFront();

    //当闪光灯处于自动模式时
    void onFlashAuto();
    //当闪光灯亮着的时候
    void onFlashOn();
    //当闪光灯熄灭时
    void onFlashOff();

    //如果相机已经准备好拍照了
    void onCameraSetupForPhoto();
    //如果相机准备好拍视频
    void onCameraSetupForVideo();

    //当摄像头状态为“准备录制视频”时
    void onRecordStateVideoReadyForRecord();
    //当摄像头状态为“录制视频”时
    void onRecordStateVideoInProgress();
    //当相机状态为“准备好拍照”时
    void onRecordStatePhoto();

    //旋转屏幕/摄像机后
    void shouldRotateControls(int degrees);

    void onStartVideoRecord(File outputFile);
    void onStopVideoRecord();
});
```

//文本  
CameraFraction可以用当前记录的持续时间提示`CameraFragmentTextListener`

//小工具  
CameraFraction带有一些默认视图

`RecordButton`, `MediaActionSwitchView`, `FlashSwitchView`, `CameraSwitchView`, `CameraSettingsView`




#### 测试信息
CodeCheck代码测试无异常  
CloudTest代码测试无异常  
病毒安全检测通过  
当前版本demo功能与原组件基本无差异


#### 版本迭代
* 1.0.1

#### 版权和许可信息

    Copyright 2017 florent37, Inc.
    
    Licensed under the Apache License, Version 2.0 (the "License");
    you may not use this file except in compliance with the License.
    You may obtain a copy of the License at
    
       http://www.apache.org/licenses/LICENSE-2.0
    
    Unless required by applicable law or agreed to in writing, software
    distributed under the License is distributed on an "AS IS" BASIS,
    WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
    See the License for the specific language governing permissions and
    limitations under the License.
