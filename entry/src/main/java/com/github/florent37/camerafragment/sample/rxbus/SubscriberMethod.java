/*
 * Copyright (C) 2021 Huawei Device Co., Ltd.
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 */

package com.github.florent37.camerafragment.sample.rxbus;

import com.github.florent37.camerafragment.internal.utils.Log;

import java.lang.reflect.InvocationTargetException;
import java.lang.reflect.Method;

/**
 * 订阅者方法
 *
 * @since 2021-03-18
 */
public class SubscriberMethod {
    private Method method;
    private ThreadMode threadMode;
    private Class<?> eventType;
    private Object subscriber;
    private int code;

    public Method getMethod() {
        return method;
    }

    public void setMethod(Method method) {
        this.method = method;
    }

    public ThreadMode getThreadMode() {
        return threadMode;
    }

    public void setThreadMode(ThreadMode threadMode) {
        this.threadMode = threadMode;
    }

    public Class<?> getEventType() {
        return eventType;
    }

    public void setEventType(Class<?> eventType) {
        this.eventType = eventType;
    }

    public Object getSubscriber() {
        return subscriber;
    }

    public void setSubscriber(Object subscriber) {
        this.subscriber = subscriber;
    }

    public int getCode() {
        return code;
    }

    public void setCode(int code) {
        this.code = code;
    }

    /**
     * 调用方法
     *
     * @param obj 参数
     */
    public void invoke(Object obj) {
        try {
            Class[] parameterType = method.getParameterTypes();
            if (parameterType.length == 1) {
                method.invoke(subscriber, obj);
            } else if (parameterType == null || parameterType.length == 0) {
                method.invoke(subscriber);
            }
        } catch (IllegalAccessException exp) {
            Log.error("invoke", " IllegalAccessException " + exp.getLocalizedMessage());
        } catch (InvocationTargetException exp) {
            Log.error("invoke", " InvocationTargetException " + exp.getLocalizedMessage());
        }
    }
}
